var Promise = require('bluebird');

/**
 * 缩放渐入
 * @param target
 */
exports.normalScaleIn = function(target){
	var defer = Promise.defer();
	target.css({
		'transform':'scale(0.8)',
		'opacity': 0
	}).transition({
		'transform':'scale(1)',
		'opacity': 1
	}, 150, 'in', function () {
		defer.resolve();
	});
	return defer.promise;
};

/**
 * 缩放淡出
 * @param target
 * @returns {defer.promise|*}
 */
exports.normalScaleOut = function(target){
	var defer = Promise.defer();
	target.transition({
		'transform':'scale(0.7)',
		'opacity': 0
	}, 150, 'out', function(){
		defer.resolve();
	});
	return defer.promise;
};

exports.fadeIn = function(target, time){
	time = time || 800;
	var defer = Promise.defer();
	target.css('opacity','0').transition({
		'opacity': 1
	}, time, 'in', function(){
		defer.resolve();
	});
	return defer.promise;
};

exports.fadeOut = function(target, time){
	time = time || 800;
	var defer = Promise.defer();
	target.transition({
		'opacity': 0
	}, time, 'out', function(){
		defer.resolve();
	});
	return defer.promise;
};

/**
 * 从左边展开
 * @param target
 */
exports.slideInLeft = function(target, w) {
	var defer = Promise.defer();
	target.css('width', 0).transition({
		'width': w
	}, 150, 'in', function(){
		defer.resolve();
	});
	return defer.promise;
};

/**
 * 收起到左侧
 * @param target
 */
exports.slideOutLeft = function(target) {
	var defer = Promise.defer();
	target.transition({
		'width': 0
	}, 150, 'out', function(){
		defer.resolve();
	});
	return defer.promise;
};