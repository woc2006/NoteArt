//集中的ui管理辅助
var $ = window.$;

var dialogCount = 0;
exports.dialogInc = function(){
	if(dialogCount == 0){
		$('#dialog_layer').show();
	}
	dialogCount++;
};

exports.dialogDec = function(){
	dialogCount--;
	if(dialogCount == 0){
		$('#dialog_layer').hide();
	}
};