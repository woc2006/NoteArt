var $ = window.$;
var timer = require('../../core/timer');
var util = require('../../util/index');
var event = require('../../core/event');
var sound = require('../audio/sound');
var timing = require('../timing/manager');
var notify = require('../notify/ui');
var config = require('../../core/config');

var _inited = false;
var _padding = 7;

var ret = {
	init: function(){
		if(_inited){
			return;
		}
		this.lastUpdate = 0;
		this.needUpdate = true;
		this.isHover = false;
		this.speed = 10; //10x

		this.parent = $('#timeline');
		this.timeInfo = this.parent.find('#curr_time');
		this.buttons= $('#control span');
		this.timeline = this.parent.find('#timeline_area');
		this.timelineCurr = this.parent.find("#timeline_bar_curr");
		this.timeBar = this.parent.find("#timeline_bar_icon");

		this.updateSize();
		this.onUpdate();
		this.bind();

		timer.registUpdate(this.onUpdate.bind(this), 15);
		event.bind(event.events.key.down, 'timeline', this);
		event.bind(event.events.note.load, 'timeline', this);
		event.bind(event.events.global.scroll, 'timeline', this);

		_inited = true;
	},

	bind: function() {
		var self = this;
		this.parent.on('click', '.g_click', function(){
			var target = $(this);
			var type = target.data('action');
			if(type == 'add'){
				self.changeSpeed(self.speed + 1);
			}else if(type == 'minus'){
				self.changeSpeed(self.speed - 1);
			}else if(type == 'play'){
				sound.toggle();
				self.updateStatus();
			}else if(type == 'stop'){
				sound.stop();
			}else if(type == 'speed'){
				$('#time_speed').toggle();
			}else if(type == 'jumpto'){
				$('#time_jumpto').toggle();
			}
		});

		this.timeline.mousemove(function(e){
			if(!self.isHover){
				self.timeInfo.addClass('hov');
			}
			self.isHover = true;
			self.needUpdate = true;
			self.hoverOffset = e.offsetX - _padding;
		}).mouseleave(function(e){
			self.isHover = false;
			self.timeInfo.removeClass('hov');
		}).mouseup(function(e){
			var x = e.offsetX - _padding;
			var time = util.lerp(0, sound.getMaxLength(), x, self.lineWidth);
			self.seekTo(time);
		});

		$("#time_jumpto").keypress(function(e){
			 var keycode = e.which;
			 if(keycode === 13){
			 	self.jumpTo();
			 }
		});
	},

	seekTo: function(time){
		sound.seekTo(time);
	},

	jumpTo : function(time){
		var val = $("#jumpto_val").val();
		$('#jumpto_val').val("")
		var maxVal = util.lerp(0, sound.getMaxLength(), this.lineWidth, this.lineWidth);
		if(!val){
			return;
		}
		var jumpVal = 0;
		if(/^[0-9]+(\.[0-9]+){0,1}$/.test(val)){
			//毫秒
			jumpVal = parseFloat(val);
		}else if(/^[0-9]+:[0-6][0-9](\.[0-9]+){0,1}$/.test(val)){
			//形如12:43.123的时间表示
			var numArray = val.split(":");
			var minutes = parseFloat(numArray[0]);
			var seconds = parseFloat(numArray[1]);
			jumpVal = (minutes * 60 + seconds) * 1000;
		}else if(/^[0-9]+:[0-9]\/[0-9]$/.test(val)){
			var beatArr = val.split(/:|\//);
			for(var i=0;i<beatArr.length;i++){
				beatArr[i] = parseInt(beatArr[i]);
			}
			if(beatArr[0] === 0){
				return;
			}
			beatArr[0] -= 1;
			jumpVal = timing.beatToTime(beatArr);
		}else{
			notify.addNotify({
				msg: config.i18n("err09")
			});
			return;
		}
		if(jumpVal && jumpVal < maxVal){
			this.seekTo(jumpVal);
		}
		else{
			notify.addNotify({
				msg: config.i18n("err10")
			});
			return;
		}
		$('#time_jumpto').hide();
	},

	changeSpeed: function(speed) {
		if(speed > 10){
			speed = 10;
		}else if(speed < 5){
			speed = 5;
		}
		sound.setSpeed(speed / 10.0);
		this.speed = speed;
		if(speed == 10){
			this.buttons.eq(2).html('1x');
		}else{
			this.buttons.eq(2).html('.'+speed+'x');
		}
	},

	updateSize: function(){
		var rect = this.timeline[0].getBoundingClientRect();
		this.width = Math.round(rect.right - rect.left);
		this.height = Math.round(rect.bottom - rect.top);
		this.lineWidth = this.width - _padding * 2; //左右7px
	},

	updateStatus: function(){
		var time = sound.stableTime();
		var len = sound.getMaxLength();
		if(this.isHover){
			var _time = util.lerp(0, len, this.hoverOffset, this.lineWidth);
			this.timeInfo.html(util.getFormatTime(_time));
		}else{
			this.timeInfo.html(util.getFormatTime(time));
		}
		if(sound.isPlaying()){
			this.buttons.eq(0).addClass('press');
		}else{
			this.buttons.eq(0).removeClass('press');
		}
	},

	onUpdate: function(){
		var time = sound.stableTime();
		var len = sound.getMaxLength();
		if(!this.needUpdate && this.lastUpdate == time){
			return;
		}
		this.needUpdate = false;
		this.lastUpdate = time;

		var w = time / len * this.lineWidth;
		this.timelineCurr.css('width',w+'px');
		this.timeBar.css("left", w+'px');

		this.updateStatus();
	},

	onEvent: function(e, param){
		if(e == event.events.global.scroll){
			sound.toggle();
			this.updateStatus();
			return event.result.stopAll;
		}else if(e == event.events.note.load){
			this.changeSpeed(10);
			sound.seekTo(0);  //todo 要不要记住上次关闭时间?
		}

	}
};

module.exports = ret;