var event = require('../../core/event');
var sound = require('../audio/sound');

/**
 * 用来监听各种事件分发和做测试的入口
 */


var ret = {
	init: function(){
		event.bindOwner("key", 'test', this);
		event.bindOwner('note', 'test', this);
		this.speed = 1;
	},

	onEvent: function(e, param){
		if(e == event.events.key.up) {
			console.log("key up:" + param.key);
		/*	if (param.key == 192) {  // `
				sound.fmod.selectAudio(0);
				sound.fmod.addCell();
				sound.fmod.volumeCell(30);
				sound.fmod.offsetCell(0);
				sound.fmod.selectAudio(1);
				for (var i = 0; i < 512; i++) {
					sound.fmod.addCell();
					sound.fmod.volumeCell(100);
					sound.fmod.offsetCell(i * 60000 / 133.005 + 337.45);
				}
				sound.fmod.setLenTrack(1000 * 15);
				sound.fmod.playTrack();  //测试合成播放
			} else if (param.key == 49) {   //1
				sound.fmod.playTrack();  //测试二次播放
			} else if (param.key == 50) {   //2
				sound.fmod.setLenTrack(88755);
				sound.fmod.seekTrack(52666);	// 这个会seek到节拍器音效的前面
				sound.fmod.resumeTrack();  //测试seek
			} else if (param.key == 51) {   //2
				sound.fmod.setLenTrack(88755);
				sound.fmod.seekTrack(52671);	// 这个会seek到节拍器音效的中间
				sound.fmod.resumeTrack();  //测试seek
			} else if (param.key == 52) {   //4
				sound.fmod.setLenTrack(88755);
				sound.fmod.seekTrack(52686);	// 这个会seek到节拍器音效的静音段
				sound.fmod.resumeTrack();  //测试seek
			} else if (param.key == 56) {   //8
				sound.fmod.setLenTrack(274400);
				this.speed = this.speed * 0.87168554287173568296447932235005;
				if (this.speed < 0.5) {
					this.speed = this.speed * 3;
				}
				sound.fmod.speedTrack(this.speed);  //测试变速
			} else if (param.key == 57) {   //9
				sound.fmod.setLenTrack(274400);
				this.speed = this.speed * 1.1472026904398770894730586135366;
				if (this.speed > 1.5) {
					this.speed = this.speed / 3;
				}
				sound.fmod.speedTrack(this.speed);  //测试变速
			} else if (param.key == 48) {   //0
				sound.fmod.toggleTrack();   //测试暂停
			}*/
		}else if(e == event.events.key.down){
			//console.log("key down:"+param.key);
		}else if(e == event.events.note.load){
			console.log("note load");
		}
	}
};

module.exports = ret;