var event = require('../../core/event');
var config = require('../../core/config');
var notify = require('../notify/ui');
var note = require('../note/manager');
var util = require('../../util/index');
var path = require('path');

var _inited = false;

var ret = {
	init: function(){
		if(_inited){
			return;
		}
		event.bindOwner('test', 'test', this);
		_inited = true;
	},

	onEvent: function(e, param){
		if(e == event.events.test.start){
			this.start();
			return event.result.stopAll;
		}else if(e == event.events.test.end){

		}
	},

	/**
	 * 启动测试
	 */
	start: function(){
		//1. 快速导出到test目录
		var _path = config.getMaPath();
		if(!_path){
			notify.addNotify({
				msg: config.i18n('err04')
			});
			return;
		}
		var ui = require('./dialog');
		ui.setManager(this);
		ui.init();
		ui.show();
	},

	/**
	 * 实际启动
	 * @private
	 */
	_doStart: function(name){
		var _path = config.getMaPath();
		_path = path.dirname(_path) + '/test/' + name + '/';
		util.mkdir(_path);
		note.exportTest(_path).then(function() {
			//启动ma
			var cp = require('child_process');
			cp.execFile(config.getMaPath());
		}).catch(function() {
			notify.addNotify({
				msg: config.i18n('err06')
			});
		});
	}
};

module.exports = ret;