var $ = window.$;
var jade = require('jade');
var animate = require('../../util/animate');
var event = require('../../core/event');
var util = require('../../util/index');
var uiUtil = require('../../util/ui');
var config = require('../../core/config');
var grid = require('../grid/manager');
var timing = require('../timing/manager');
var slide = require('../../core/slide');
var sound = require('../audio/sound');

var _inited = false;

/**
 * sample编辑
 */

var ret = {
	init: function(){
		if(_inited){
			return;
		}
		this.type = 0;
		this.slideId = [];
		event.bind(event.events.key.up, 'sample_edit', this);
		event.lock('key','sample_edit');
		_inited = true;
	},

	setManager: function (ma) {
		this.manager = ma;
	},

	onEvent: function(e, param){
		if(param.key == 27){
			this.hide();
			return event.result.stopAll;
		}
	},

	save: function () {
		var offset = this.parent.find("#dialog_edit_offset").val();
		offset = Math.round(parseInt(offset));
		offset = util.clamp(offset, -1000, 1000);
		for(var i=0;i<this.param.length;i++){
			var note = this.param[i];  //一定存在
			note.offset = offset;
			sound.offsetCell(note.tid, timing.beatToTime(note.beat) + offset);
		}
		event.trigger(event.events.note.change);
	},

	show: function(param){
		if(this.parent){
			return;
		}
		//参数准备, param外部已检验
		this.param = param;
		var tmpl = jade.compileFile('./tmpl/sample/edit.jade');
		var conf = {
			config: config,
			num: param.length
		};
		var html = tmpl(conf);
		$('#dialog_layer').append(html);

		var self = this;
		this.parent = $('#dialog_edit');
		this.parent.on('click','.g_barhead i', function () {
			self.hide();
		});
		this.parent.on('click','.g_click', function(){
			var type = $(this).data('type');
			if(type == 'close'){
				self.save();
				self.hide();
			}
		});

		var sliders = this.parent.find('.g_slide');
		var inputs = this.parent.find('input');
		sliders.eq(0).attr('id', 'dialog_edit_offset_s');

		var id = slide.on({
			parent: sliders.eq(0),
			min: -1000,
			max: 1000,
			onchange: function (val) {
				inputs.eq(0).val(Math.round(val));
			}
		});
		this.slideId.push(id);

		inputs.keyup(function (e) {
			var target = $(this);
			slide.setVal(self.slideId[0], parseInt(target.val()));
		});

		this.parent.show();
		uiUtil.dialogInc();
		animate.normalScaleIn(this.parent).then(function(){
			if(param.length == 1){
				//只有一个元素的情况, 要读出属性值
				var note = param[0];  //一定存在
				slide.setVal(self.slideId[0], note.offset || 0);
				inputs.eq(0).val(note.offset || 0);
			}else{
				slide.setVal(self.slideId[0], 0);
				inputs.eq(0).val(0);
			}
		});
		event.trigger(event.events.global.blur);
	},

	hide: function(){
		var self = this;
		if(!this.parent){
			return;
		}
		animate.normalScaleOut(this.parent).then(function(){
			self.parent.remove();
			self.parent = null;
			uiUtil.dialogDec();
			event.unbind(event.events.key.up, 'sample_edit');
			slide.off(self.slideId[0]);
		});
		event.unlock();
		_inited = false;
	}
};

module.exports = ret;