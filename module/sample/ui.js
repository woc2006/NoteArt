var $ = window.$;
var jade = require('jade');
var event = require('../../core/event');
var sound = require('../audio/sound');
var notify = require('../notify/ui');
var config = require('../../core/config');
var input = require('../../core/input');

var _inited = false;
var tmpl, tmpl_item;

var ret = {
	init: function(){
		if(_inited){
			return;
		}
		this.focus = false;
		this.playId = -1;
		this.parent = $('#tool_sample');

		var self = this;
		this.parent.on('mousedown', 'li', function(e){
			e.stopPropagation();

			var target = $(this);
			if(!self.focus){
				self.focus = true;
				event.trigger(event.events.global.blur, null, self);
			}
			var clickAt = $(e.target);
			if(e.which == 3){
				//删除
				if(target.hasClass('sub')){
					self.deleteSelect(target.children("div"));
				}else{
					self.deleteSelect(target);
				}
				return;
			}

			if(target.hasClass('sub')){
				//文件夹item
				if(target.hasClass('on')){
					target.removeClass('on').addClass('off');
				}else{
					target.removeClass('off').addClass('on');
				}
				return;
			}
			//置空
			if(target.hasClass('ept')){
				self.parent.find('.curr').removeClass('curr');
				self.onControl('stop');
				self.manager._setCurrent(null);
				return;
			}
			var name = target.find('span').text();
			if(e.target.tagName == 'EM') {
				//控制item
				self.onControl(clickAt.attr('class'), name);
				return true;
			}
			//选中
			if(!target.hasClass('curr')){
				self.parent.find('.curr').removeClass('curr');
				target.addClass('curr');
				self.manager._setCurrent(name);
				self.onControl('stop');
			}

			//双击高亮
			if(input.isCtrl()){
				event.trigger(event.events.grid.focus, {sound: name, noseek: true});
			}
			return true;
		});
		//event.bind(event.events.key.up, 'sample', this);
		event.bind(event.events.global.blur, 'sample', this);

		_inited = true;
	},

	setManager: function(ma){
		this.manager = ma;
	},

	onEvent: function(e, param){
		if(e == event.events.global.blur){
			if(this.focus){
				this.focus = false;
				this.onControl('stop');
			}
		}
	},

	sync: function(){
		this.clear();
		this.parent.append('<li class="ept">Empty</li>');

		var list = this.manager._getList();
		if(!list || list.length == 0){
			return;
		}
		if(tmpl == null){
			tmpl = jade.compileFile('./tmpl/sample/list.jade');
		}
		var html = tmpl(list);
		this.parent.append(html);
	},

	syncSingle: function(item){
		if(tmpl_item == null){
			tmpl_item = jade.compileFile('./tmpl/sample/item.jade');
		}
		var html = tmpl_item(item);
		this.parent.append(html);
	},

	clear: function(){
		this.parent.html('');
	},

	deleteSelect: function(target){
		var name = target.find('span').text();
		if(!this.manager.removeSample(name)){
			return;
		}
		this.manager._setCurrent(null);
		if(target[0].tagName == 'DIV'){
			target.parent().remove();
		}else{
			target.remove();
			this.onControl('stop');  //如果有playId, 那一定是这个sound的
		}

	},

	onControl: function(action, file){
		if(this.playId != -1){
			sound.fmod.selectAudio(this.playId);
			sound.fmod.stopAudio();
			this.playId = -1;
		}
		if(action == 'play'){
			if(!file){
				file = this.manager.getCurrent();
			}
			this.playId = sound.load(this.manager.getFilePath(file));
			if(this.playId == -1){
				notify.addNotify({
					msg: config.i18n("err01")
				});
				return;
			}
			sound.fmod.stopAudio();
			sound.fmod.volumeAudio(30);
			sound.fmod.playAudio();
		}

	}
};

module.exports = ret;