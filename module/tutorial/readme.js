var $ = window.$;
var jade = require('jade');
var animate = require('../../util/animate');
var util = require('../../util/index');
var uiUtil = require('../../util/ui');
var io = require('../io/ui');
var notify = require('../notify/ui');
var event = require('../../core/event');
var config = require('../../core/config');
var slide = require('../../core/slide');


var _init = false;


var ret = {
    init: function(){
        if(_init){
            return;
        }
        //event.lock('key', 'readme_ui');
        event.bind(event.events.dialog.readme, 'readme', this);
        _init = true;
    },

    setManager: function(ma){
        this.manager = ma;
    },

    onEvent: function(e, param) {
        if(e == event.events.dialog.readme){
            this.show();
            return true;
        }
    },

    show: function(isNew){
        var note = require('../note/manager').getNote('main');
        var mode = note.meta.mode;
        //暂时屏蔽除了pad模式的单图指引
        if(!mode || mode != 4){
            return;
        }
        $("#readme_img").attr("src", "static/img/guide/guide_pad.jpg").show().click(function(){
           $(this).hide();
        });
    }
};

module.exports = ret;