var $ = window.$;
var document = window.document;
var event = require('../../core/event');
var config = require('../../core/config');
var _inited = false;

var ret = {
	init: function(){
		if(_inited){
			return;
		}		
		//event.bind(event.events.tutorial.show,'tutorial', this);
		this.tutorial_conf = require('../../data/tutorial/tutorial.json');
		this.tutorial_def = require('../../data/tutorial/en.json');
		if(!config.isDefaultLang()){
			this.tutorial_local = require('../../data/tutorial/'+config.getVal(config.ITEM.LANG)+'.json');
		}
		_inited = true;
	},

	getData : function(key,index){
		if(!this.tutorial_conf){
			this.init();
		}
		var data = this.tutorial_conf[key];
		if(!data){
			return null;
		}
		var data = data[index];
		if(!data){
			return null;
		}
		var i18n_key = key + "_" + index;
		var content = this.tutorial_def[i18n_key];
		if(!content){
			return null;
		}
		if(this.tutorial_local && this.tutorial_local[i18n_key]){
			content = this.tutorial_local[i18n_key];
		}
		data.content = content;
		return data;
	},

	onEvent: function(e, param){
		if(e == event.events.tutorial.show){
			if(param){
				if(typeof(param) === "string"){
					this.drawTutorial(param);
				}else{
					this.drawTutorial(param.id);
				}
			}
			return true;
		}
		return false;
	},

	drawTutorial : function(key){
		this.key = key;
		this.index = 0;
		this.drawNextTutorial();
	},

	triggerCallback : function(){
		event.trigger(event.events.tutorial.end);
	},

	drawNextTutorial : function(){
		var note = require('../note/manager');
		var chart = note.getNote('main');
		var mode = chart.meta.mode;

		//跳过特定的提示
		if(mode == 3){
			if(this.index == 3){
				this.index++;
			}
		}

		var data = this.getData(this.key,this.index);
		if(data){
			this.drawContents(data);
			this.index++;
		}
		else{
			$("#tutorial_content,#canvas_tutorial").remove();
			this.triggerCallback();
		}
	},

	//绘制一个全黑的矩形 但是x,y起点w宽h高的矩形被抠掉
	drawCanvas: function(opts){
		if(!opts.position || opts.position.length != 4){
			return;
		}
		var cavX = opts.position[0];
		var cavY = opts.position[1];
		var cavW = opts.position[2];
		var cavH = opts.position[3];
		var canvas = $('#canvas_tutorial');
		if(!canvas.size()){
			canvas = $('<canvas id="canvas_tutorial"></canvas>');
			var w = window.innerWidth;
			var h = window.innerHeight;
			canvas.css({
				position: 'absolute',
				zIndex:23333,
				top:0,
				left:0
			}).attr('width', w).attr('height', h);
			$('body').append(canvas);
		}

	    var ctx = canvas[0].getContext('2d');
	    ctx.fillStyle = 'rgba(0,0,0,0.6)';
	    ctx.fillRect(0, 0, w, h);
	    ctx.clearRect(cavX, cavY, cavW, cavH);
	},

	//绘制帮助内容部分 如果文字尺寸过大必须手动指定width或者height的一个值（里面再放一个div）
	drawContents : function(opts){
		this.drawCanvas(opts);
		var body = $("body");
		if(!opts.text || !opts.text.position || opts.text.position.length != 4){
			return;
		}
		var pos = opts.text.position; //pos的参数依次为x y width height
		var cX = pos[0];
		var cY = pos[1];
        $("#tutorial_content").remove();
		body.append('<span id="tutorial_content"><div class="next-hint"><span class="hint-text"></span></div></span>');
		var content = $("#tutorial_content");
		content.prepend(opts.content);
        content.children(".next-hint").children(".hint-text").html(config.i18n("tutorial01"));
		content.css({
			top : cY + "px",
			left : cX + "px"
		});
		if(pos[2]){
			content.css("width",pos[2] + "px");
		}
		if(pos[3]){
			content.css("height",pos[3] + "px");
		}
		var self = this;
		$("#tutorial_content,#canvas_tutorial").click(function(){
			self.drawNextTutorial();
		});
	}
};

module.exports = ret;