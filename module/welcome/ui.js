var $ = window.$;
var jade = require('jade');
var animate = require('../../util/animate');
var event = require('../../core/event');
var uiUtil = require('../../util/ui');
var config = require('../../core/config');
var open = require("open");


var tmpl, tmpl_list;
var _init = false;

var ret = {
	init: function(){
		if(_init){
			return;
		}
		tmpl = jade.compileFile('./tmpl/welcome/dialog.jade');
		tmpl_list = jade.compileFile('./tmpl/welcome/list.jade');
		var html = tmpl({config: config});
		var self = this;
		$('#dialog_layer').append(html);
		this.parent = $('#dialog_welcome');
		this.parent.on('click','.g_barhead i', function () {
			self.hide();
		});
		this.parent.on('click','.g_click', function(){
			var type = $(this).data('type');
			if(type == 'open'){
				//self.hide();
				event.bindOnce(event.events.global.dialog, 'welcome_ui', self);
				event.trigger(event.events.file.load);
			}else if(type == 'new'){
				self.hide();
				event.trigger(event.events.dialog.newmeta);
			}else if(type == 'import'){
				//self.hide();
				event.bindOnce(event.events.global.dialog, 'welcome_ui', self);
				event.trigger(event.events.file.imports);
			}
		});
		this.parent.on('click', 'li', function(e){
            var target = $(e.target);
            if(target.parents(".files").length == 1){
                var target = $(this).data('idx');
                self.hide();
                self.manager._pickAt(+target);
            }else if(target.parents(".links").length == 1){
                var url = $(this).attr("data-url");
                if(url){
                    open(url);
                }
            }
		});
        // fast guide links will be read from localization file
        // make sure the numbers are continous and don't put 0 before numbers
        var n = 0;
        var desc;
        var guideHtml = "";
        while(true){
            desc = config.i18n("wlink_desc" + n);
            if(!desc){
                break;
            }
            guideHtml += "<li data-url='" + config.i18n("wlink_url" + n) + "'>" + desc + "</li>";
            n++;
        }
        this.parent.find(".links").children("ul").html(guideHtml);
		event.bind(event.events.key.up, 'welcome_ui', this);
		event.lock('key', 'welcome_ui');
		_init = true;
	},

	setManager: function(ma){
		this.manager = ma;
	},

	onEvent: function(e, param){
		//if(param.key == 27){
		//	this.hide();
		//}
		if(e == event.events.global.dialog){
			this.hide();
		}else if(e == event.events.key.up){
			if(param.key == 27){
				this.hide();
			}
		}
	},

	show: function(){
		if(!_init){
			this.init();
		}
		var list = this.manager._getHistory();
		var html = tmpl_list(list);
		this.parent.find('.files').children("ul").html(html);
		this.parent.show();
		uiUtil.dialogInc();
		animate.normalScaleIn(this.parent);
	},

	hide: function(){
		var self = this;
		animate.normalScaleOut(this.parent).then(function(){
			self.parent.remove();
			self.parent = null;
			uiUtil.dialogDec();
			event.unbindByKey('welcome_ui');
		});
		event.unlock();
		tmpl = null;
		tmpl_list = null;
		_init = false;
	}
};

module.exports = ret;