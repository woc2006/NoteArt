var ui = require('./ui');
var event = require('../../core/event');
var Log = require('../../util/logger');
var note = require('../note/manager');
var util = require('../../util/index');

var _inited = false;

var ret = {
	/**
	 * 初始化，加载数据，绑定事件
	 */
	init: function(){
		if(_inited){
			return;
		}
		this.chart = null;
		this.table = null;

		event.bind(event.events.note.load, 'review', this);
		event.bind(event.events.note.unload, 'review', this);
		ui.setManager(this);
		ui.init();

		_inited = true;
	},

	onEvent: function(e, param) {
		if(e == event.events.note.load){
			this.chart = note.getNote(param.key);
			this.table = util.getJsonChainValue(this.chart, 'extra.review');
			if(!this.table){
				util.setJsonChainValue(this.chart, 'extra.review', []);
				this.table = this.chart.extra.review;
			}
			ui.sync();
		}else if(e == event.events.note.unload){
			this.chart = null;
			this.table = null;
			ui.clear();
		}
	},


	/**
	 * 增加新评注
	 * @param param
	 * @returns {number}
	 * @private
	 */
	_add: function(param){
		if(!this.table){
			return;
		}

		this.table.push(param);
		event.trigger(event.events.note.change);
	},

	/**
	 * @param param
	 * @private
	 */
	_change: function(idx, param){
		if(!this.table){
			return;
		}
		this.table[idx] = param;
		event.trigger(event.events.note.change);
	},

	/**
	 * @param idx
	 * @private
	 */
	_remove: function(idx) {
		if(!this.table){
			return;
		}
		this.table.splice(idx, 1);

		event.trigger(event.events.note.change);
	},

	_getReview: function(idx) {
		return this.table[idx];
	},

	_getReviews: function(){
		return this.table;
	}

};

module.exports = ret;