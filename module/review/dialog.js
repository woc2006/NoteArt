var $ = window.$;
var jade = require('jade');
var animate = require('../../util/animate');
var event = require('../../core/event');
var util = require('../../util/index');
var uiUtil = require('../../util/ui');
var config = require('../../core/config');


var _inited = false;

var ret = {
	init: function(){
		if(_inited){
			return;
		}
		event.bind(event.events.key.up, 'review_edit', this);
		event.lock('key', 'review_edit');

		_inited = true;
	},

	setManager: function (ma) {
		this.manager = ma;
	},

	onEvent: function(e, param){
		if(param.key == 27){
			this.hide();
			return event.result.stopAll;
		}
	},

	save: function () {
		this.param.param.type = +this.parent.find('.select label.curr').data('idx');
		this.param.param.content = this.parent.find('#dialog_review_cont').val();
		//附加选中的note
		var note = require('../grid/manager');
		var notes = note.getSelectIds({allMode: true});
		if(notes.length){
			this.param.param.note = notes;
		}
		this.manager._onDialogResult(this.param);
	},

	show: function(param){
		//参数准备, param外部已检验
		this.param = param;
		if(!param){
			this.param = {
				idx: -1,
				param:{
					type: 0,
					content: ''
				}
			}
		}
		var tmpl = jade.compileFile('./tmpl/review/dialog.jade');
		var html = tmpl({config: config, param: this.param.param});
		$('#dialog_layer').append(html);

		var self = this;
		this.parent = $('#dialog_review');
		this.parent.find('.select label').eq(this.param.param.type).addClass('curr');
		this.parent.on('click','.g_barhead i', function () {
			self.hide();
		});
		this.parent.on('click','.g_click', function(){
			var type = $(this).data('type');
			if(type == 'close'){
				self.save();
				self.hide();
			}
		});

		this.parent.find('.select').on('click', 'label', function() {
			var target = $(this);
			self.parent.find('.select label.curr').removeClass('curr');
			target.addClass('curr');
		});

		this.parent.show();
		uiUtil.dialogInc();
		animate.slideInLeft(this.parent, 250);  //css值
		//event.trigger(event.events.global.blur);
	},

	hide: function(){
		var self = this;
		if(!this.parent){
			return;
		}
		animate.slideOutLeft(this.parent).then(function(){
			self.parent.remove();
			self.parent = null;
			uiUtil.dialogDec();
			event.unbind(event.events.key.up, 'review_edit');
		});
		event.unlock();
		_inited = false;
	}
};

module.exports = ret;