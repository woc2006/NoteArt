var ui = require('./ui');
var event = require('../../core/event');
var Log = require('../../util/logger');
var note = require('../note/manager');
var util = require('../../util/index');

var _inited = false;

var ret = {
	/**
	 * 初始化，加载数据，绑定事件
	 */
	init: function(){
		if(_inited){
			return;
		}
		this.chart = null;
		this.current = null;
		this.table = null;

		event.bind(event.events.note.load, 'layer', this);
		event.bind(event.events.note.unload, 'layer', this);
		ui.setManager(this);
		ui.init();
		Log.log('Layer init');

		_inited = true;
	},

	onEvent: function(e, param) {
		if(e == event.events.note.load){
			this.chart = note.getNote(param.key);
			this.table = util.getJsonChainValue(this.chart, 'extra.color');
			if(!this.table){
				util.setJsonChainValue(this.chart, 'extra.color', {});
				this.table = this.chart.extra.color;
			}
			ui.sync();
		}else if(e == event.events.note.unload){
			this.chart = null;
			this.table = null;
			this.current = null;
			ui.clear();
		}
	},

	_getLayers: function() {
		return this.table;
	},

	/**
	 * 增加新颜色, 去重
	 * @param param
	 * @returns {number}
	 * @private
	 */
	_add: function(param){
		if(!this.table){
			return -1;
		}
		if(!param.color){
			return -1;
		}
		//找到id最大值, 去重
		var _max = -1;
		for(var key in this.table){
			if(!this.table.hasOwnProperty(key)){
				continue;
			}
			var _key = +key;
			if(this.table[key].color == param.color){
				return -1;
			}
			if(_key > _max){
				_max = _key;
			}
		}
		_max += 1;
		this.table[_max] = {
			color: param.color,
			name: param.name
		};
		this.syncNote(_max);
		event.trigger(event.events.note.change);
		return _max;
	},

	/**
	 * 更改指定idx的颜色, 并同步给所有note
	 * @param param
	 * @private
	 */
	_change: function(param){
		var layer = this.getLayer(param.idx);
		if(!layer){
			return;
		}
		layer.name = param.name;
		if(layer.color != param.color){
			layer.color = param.color;
			event.trigger(event.events.grid.layer, true);
		}
		event.trigger(event.events.note.change);
	},

	/**
	 * 删除颜色, 同步改动给notes
	 * @param idx
	 * @private
	 */
	_remove: function(idx) {
		if(!this.table){
			return;
		}
		delete this.table[idx];
		var notes = this.chart.note;
		for(var i=0;i<notes.length;i++){
			if(notes[i].color == idx){
				delete notes[i].color;
			}
		}
		event.trigger(event.events.note.change);
		event.trigger(event.events.grid.layer, true);
	},

	/**
	 * 选中颜色, 同步给当前选中的note
	 * @param idx
	 * @private
	 */
	_setCurrent: function(idx){
		if(!this.table){
			return;
		}
		if(idx == null){
			this.current = null;
		}else{
			this.current = idx;
		}
		this.syncNote(idx);
	},

	syncNote: function(idx){
		event.trigger(event.events.grid.assign, {color: idx});
		event.trigger(event.events.grid.layer, false);
	},

	getCurrent: function(){
		return this.current;
	},

	getLayer: function(idx){
		if(!this.table){
			return;
		}
		if(typeof idx != 'number'){
			return null;
		}
		if(!this.table[idx]){
			return null;
		}
		return this.table[idx];
	},

	getColor: function(idx){
		var ly = this.getLayer(idx);
		if(ly){
			return ly.color;
		}
		return null;
	}
};

module.exports = ret;