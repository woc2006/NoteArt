var $ = window.$;
var jade = require('jade');
var animate = require('../../util/animate');
var event = require('../../core/event');
var util = require('../../util/index');
var uiUtil = require('../../util/ui');
var config = require('../../core/config');
var notify = require('../notify/ui');
var needle = require('needle');

var _inited = false;

/**
 * 登录, 入口在launcher
 */

var ret = {
	init: function(){
		if(_inited){
			return;
		}
		event.bind(event.events.key.up, 'user', this);
		event.bind(event.events.dialog.user, 'user', this);
		event.lock('key', 'user');
		_inited = true;
	},

	onEvent: function(e, param){
		if(e == event.events.key.up){
			if(param.key == 27){
				this.hide();
				return event.result.stopAll;
			}
		}else if(e == event.events.dialog.user){
			this.show();
			return event.result.stopAll;
		}
	},

	login: function(){
		var name = this.parent.find("#dialog_name").val();
		var psw = this.parent.find("#dialog_psw").val();
		if(!name || !psw){
			return;
		}
		var _str = psw+"1";  //把html字符串传给node函数似乎有问题
		_str = _str.substring(0, psw.length);  //需要通过某种办法强转成node字符串
		psw = util.md5(_str);
		var data = {
			name: name,
			psw: psw,
			from: 1
		};
		var self = this;
		notify.showWaiting(true);
		needle.request('post', config.getServer()+'/cgi/login', data, {proxy:config.getVal(config.ITEM.PROXY)}, function(err, resp){
			if(!err && resp.statusCode == 200){
				var raw = JSON.parse(resp.body);
				if(raw.code == 0){
					config.update(config.ITEM.USER, name);
					config.update(config.ITEM.PSW, psw);
					config.update(config.ITEM.UID, raw.data.uid);
					config.update(config.ITEM.SID, raw.data.key);
					$('#action_login').html(config.i18n("usr04")+":"+name);
					event.trigger(event.events.user.login);
					self.hide();
				}else{
					notify.addNotify({
						msg: config.i18n("err11")
					});
				}

			}else{
				notify.addNotify({
					msg: config.i18n("err11")
				});
			}
			notify.showWaiting(false);
		})
	},

	logout: function () {
		var sid = config.getVal(config.ITEM.SID);
		var uid = config.getVal(config.ITEM.UID);
		if(!sid || !uid){
			return;
		}
		var self = this;
		notify.showWaiting(true);
		needle.request('post', config.getServer()+'/cgi/logout?'+config.getHttpSession(), {}, {proxy:config.getVal(config.ITEM.PROXY)}, function (err, resp) {
			if(!err && resp.statusCode == 200){
				self.toggle(true);
				config.update(config.ITEM.PSW, '');
				config.update(config.ITEM.UID, '');
				config.update(config.ITEM.SID, '');
				$('#action_login').html(config.i18n('usr06'));
				event.trigger(event.events.user.logout);
			}
			notify.showWaiting(false);
		});
	},

	toggle: function (showLogin) {
		if(showLogin){
			this.parent.find('.user').hide();
			this.parent.find('.login').show();
			var btn = this.parent.find(".g_button");
			btn.eq(0).hide();
			btn.eq(1).show();
		}else{
			this.parent.find('.user').show();
			this.parent.find('.login').hide();
			var btn = this.parent.find(".g_button");
			btn.eq(1).hide();
			btn.eq(0).show();
			this.parent.find('.name').html(config.getVal(config.ITEM.USER));
		}
	},

	show: function(){
		var tmpl = jade.compileFile('./tmpl/user/dialog.jade');
		var html = tmpl({
			config: config
		});
		$('#dialog_layer').append(html);

		var self = this;
		this.parent = $('#dialog_login');

		if(config.getVal(config.ITEM.SID) && config.getVal(config.ITEM.USER)){
			this.toggle(false);
		}else{
			this.toggle(true);
			this.parent.find('input').val('');
		}
		this.parent.on('click','.g_barhead i', function () {
			self.hide();
		});
		this.parent.on('click','.g_click', function(){
			var type = $(this).data('type');
			if(type == 'ok'){
				self.login();
			}else if(type == 'logout'){
				self.logout();
			}
		});

		this.parent.show();
		uiUtil.dialogInc();
		animate.normalScaleIn(this.parent);
		event.trigger(event.events.global.blur);
	},

	hide: function(){
		var self = this;
		if(!this.parent){
			return;
		}
		animate.normalScaleOut(this.parent).then(function(){
			self.parent.remove();
			self.parent = null;
			event.unbindByKey('user');
		});
		event.unlock();
		uiUtil.dialogDec();
		_inited = false;
	}
};

module.exports = ret;