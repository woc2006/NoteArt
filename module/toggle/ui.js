var $ = window.$;
var event = require('../../core/event');
var enums = require('../../core/enum');
var config = require('../../core/config');

var _inited = false;

var ret = {
	Tools:{
		View:0,
		Select:1,
		Sample:2,
		Edit:3,
        UbeatHold:4
	},
	init: function(){
		if(_inited){
			return;
		}
		var self = this;
		$(window.document.body).on('click', '.g_groupbutton .g_button', function (e) {
			e.stopPropagation();
			var item = $(this);
			item.parent().find('.press').removeClass('press');
			item.addClass('press');
		});

		//点击tab, 控制分页
		$('.g_tab').on('click', 'span', function(e) {
			e.stopPropagation();
			var target = $(this);
			var type = target.data('type');
			self.changeList(type);
		});
		$('#action_login').click(function (e) {
			e.preventDefault();
			event.trigger(event.events.dialog.user);
		});
		//如果登录过, 显示名称
		if(config.isLogin()){
			$('#action_login').html(config.i18n("usr04")+":"+config.getVal(config.ITEM.USER));
		}

		event.bind(event.events.key.up, 'status', this);
		_inited = true;
	},
	
	onEvent: function(e, param){
		if(!param.ctrl && !param.alt && !param.shift){
			var idx  = param.key - 49;
			if(param.key >= enums.KEY.NUM_1 && param.key <= enums.KEY.NUM_9){
				var btns = $("#edit_tool").children(".g_button");
				if(idx <= btns.length - 1){
					// 这里不能直接加press 因为点击有额外的动作在ui-catch中
					btns.removeClass("press").eq(idx).click();
				}
				return event.result.stopAll;
			}
		}
	},
	
	updateTool : function(idx){
		var tool = $("#edit_tool");
		var btns = tool.children(".g_button");
		if(idx > btns.length - 1){
			return;
		}
		btns.removeClass("press");
		btns.eq(idx).addClass("press");
	},

	changeList: function(type) {
		var target = $("#tool_right .g_tab").find('span[data-type="'+type+'"]');
		if(target.hasClass('curr')){
			return;
		}
		target.parent().parent().find('ul').hide();
		target.parent().find('.curr').removeClass('curr');
		target.addClass('curr');
		if(type == 'sample'){
			$('#tool_sample').show();
		}else if(type == 'review'){
			$('#tool_review').show();
		}else if(type == 'layer'){
			$('#tool_layer').show();
		}
	}
};

module.exports = ret;
