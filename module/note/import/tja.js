var Log = require('../../../util/logger');
var path = require('path');
var util = require('../../../util/index');
var notify = require('../../notify/ui');
var fs = require('fs');
var frac = require('../../../util/frac');
var config = require('../../../core/config');

var ret = {

	getValPair : function(line){
		var tmp = line.split(":");
		return [tmp[0].trim(), tmp[1].trim()];
	},

	parseMeta: function(filepath){
		var raw = fs.readFileSync(filepath, {encoding:'utf8'});
		if(!raw){
			return null;
		}
		var lines = raw.split(/\n|\r\n/g);
		var ret = {};
		ret.length = 90;
		for(var i=0;i<lines.length;i++){
			var line = lines[i];
			var pos = line.indexOf(":");
			if(pos == -1){
				if(line == "#START"){
					break;
				}else{
					continue;
				}
			}
			var pair = this.getValPair(line);
			if(pair[0] == "TITLE"){
				ret.title = pair[1];
			}else if(pair[0] == "ARTIST"){
				ret.artist = pair[1];
			}else if(pair[0] == "LEVEL"){
				ret.version = "Lv."+pair[1];
			}else if(pair[0] == "BPM") {
				ret.bpm = +pair[1];
			}
		}

		return ret;
	}
};

module.exports = ret;