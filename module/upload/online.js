var $ = window.$;
var jade = require('jade');
var animate = require('../../util/animate');
var event = require('../../core/event');
var util = require('../../util/index');
var uiUtil = require('../../util/ui');
var config = require('../../core/config');
var notify = require('../notify/ui');

var _inited = false;

/**
 * 上传前的online确认
 * 由下面子页组成
 * 1. 说明
 * 2. 是否覆盖一个已有Chart
 * 3. 选择在线同名song 或者创建song, 或者输入song id
 * 4. version重名确认
 */

var ret = {
	init: function(){
		if(_inited){
			return;
		}
		this.curr = 0;
		event.bind(event.events.key.up, 'upload_online', this);
		event.lock('key', 'upload_online');
		_inited = true;
	},

	setManager: function (ma) {
		this.manager = ma;
	},

	onEvent: function(e, param){
		if(param.key == 27){
			this.hide();
			return event.result.stopAll;
		}
	},

	changePage: function(page){
		this.curr = page;
		var tmpl = jade.compileFile('./tmpl/upload/page/'+page+'.jade');
		this.parent.find('.body').html(tmpl({config: config}));
		this.button.show();
		if(page == 0){
			this.button.eq(0).hide();
		}
	},

	tryChangePage: function(page){
		var self = this;
		if(page == 2){
			// 查询chart并跳转
			var pid = +this.parent.find("#dialog_chart_id").val();
			if(!pid){
				return;
			}
			this.canUpload = false;
			notify.showWaiting(true);
			this.manager._getOnlineChartInfo(pid).then(function(data) {
				if(data && data.code == 0){
					data = data.data;
					self.changePage(2);
					var lines = self.parent.find('.line');
					lines.eq(0).html('Title:'+data.title);
					lines.eq(1).html('Artist:'+data.artist);
					lines.eq(2).html('Version:'+data.version);
					if(data.uid == config.getVal(config.ITEM.UID)){
						if(data.type == 2){
							//stable谱
							self.parent.find('.inf').hide();
							self.parent.find('.err').eq(1).show();
						}else{
							var chart = self.manager._getChart();
							//用online值覆盖
							chart.meta.id = data.cid;
							chart.meta.version = data.version;
							util.setJsonChainValue(chart, 'meta.song.id', data.sid);
							util.setJsonChainValue(chart, 'meta.song.title', data.title);
							util.setJsonChainValue(chart, 'meta.song.artist', data.artist);
							event.trigger(event.events.note.change);
							self.canUpload = true;
						}
					}else{
						//作者不同
						self.parent.find('.inf').hide();
						self.parent.find('.err').eq(0).show();
					}
				}else{
					notify.addNotify({
						msg: config.i18n("err12"),
						err: true
					});
				}
				notify.showWaiting(false);
			});
			return;
		}
		if(page == 4){
			//查询song并跳转
			var pid = +this.parent.find("#dialog_song_id").val();
			if(!pid){
				return;
			}
			notify.showWaiting(true);
			this.manager._getOnlineSongInfo(pid).then(function(data) {
				if(data){
					if(data.code == 0){
						data = data.data;
						self.changePage(4);
						var lines = self.parent.find('.line');
						lines.eq(0).html('Title:'+ data.title);
						lines.eq(1).html('Artist:'+ data.artist);

						var chart = self.manager._getChart();
						util.setJsonChainValue(chart, 'meta.song.id', data.sid);
						util.setJsonChainValue(chart, 'meta.song.title', data.title);
						util.setJsonChainValue(chart, 'meta.song.artist', data.artist);
						event.trigger(event.events.note.change);
					}else if(data.code == -2){
						notify.addNotify({
							msg: config.i18n("err12"),
							err: true
						});
					}
				}
				notify.showWaiting(false);
			});
			return;
		}
		if(page == 6){
			//创建歌曲页和chart页
			var chart = this.manager._getChart();
			var conf = {
				title: chart.meta.song.title,
				artist: chart.meta.song.artist,
				version: chart.meta.version,
				mode: chart.meta.mode,
				type: 1
			};
			var grid = require('../grid/manager');
			conf.length = Math.round(grid.getMaxNoteTime() / 1000);
			var timing = require('../timing/manager');
			conf.bpm = Math.round(timing.getStartBPM());
			if(!conf.title || !conf.artist || !conf.version || !conf.length || conf.bpm == 0){
				notify.addNotify({
					msg: config.i18n("err26"),
					err: true
				});
				return;
			}

			notify.showWaiting(true);
			this.manager._createOnlineChart(conf).then(function(data) {
				if(data){
					if(data.code == 0) {
						data = data.data;
						util.setJsonChainValue(chart, 'meta.id', data.cid);
						util.setJsonChainValue(chart, 'meta.song.id', data.sid);
						event.trigger(event.events.note.change);
						self.hide();
						self.manager._toUpload();
					}else if(data.code == -1){
						notify.addNotify({
							msg: config.i18n("err27"),
							err: true
						});
					}else if(data.code == -2){
						notify.addNotify({
							msg: config.i18n("err13"),
							err: true
						});
					}else if(data.code < -10 || data.code != -1000){
						notify.addNotify({
							msg: config.i18n("err29"),
							err: true
						});
					}
				}
				notify.showWaiting(false);
			});
			return;
		}
	},

	tryFinish: function() {
		//创建chart并跳转
		var chart = this.manager._getChart();
		var conf = {
			version: chart.meta.version,
			sid: chart.meta.song.id,
			mode: chart.meta.mode,
			type: 2
		};
		var grid = require('../grid/manager');
		conf.length = Math.round(grid.getMaxNoteTime() / 1000);

		this.canUpload = false;
		notify.showWaiting(true);
		var self = this;
		this.manager._createOnlineChart(conf).then(function(data) {
			if(data){
				if(data.code == 0){
					data = data.data;
					self.changePage(5);
					var lines = self.parent.find('.line');
					lines.eq(0).html('Chart: Http://m.mugzone.net/chart/'+data.cid);

					util.setJsonChainValue(chart, 'meta.id', data.cid);
					event.trigger(event.events.note.change);
					self.hide();
					self.manager._toUpload();
				}else if(data.code == -2){
					notify.addNotify({
						msg: config.i18n("err13"),
						err: true
					});
				}else if(data.code < -10 || data.code != -1000){
					notify.addNotify({
						msg: config.i18n("err29"),
						err: true
					});
				}
			}
			notify.showWaiting(false);
		});
	},


	show: function(param){
		this.param = param;
		var tmpl = jade.compileFile('./tmpl/upload/online.jade');
		var html = tmpl({config: config});
		$('#dialog_layer').append(html);

		var self = this;
		this.parent = $('#dialog_upload_online');
		this.button = this.parent.find('.g_bt span');

		this.changePage(0);
		this.parent.on('click','.g_barhead i', function () {
			self.hide();
		});
		this.parent.on('click','.g_click', function(){
			var type = $(this).data('type');
			if(type == 'ok') {
				if(self.curr == 0) {
					self.hide();
				} else if(self.curr == 1) {
					self.tryChangePage(2);
				} else if(self.curr == 2){
					self.hide();
					if(self.canUpload){
						self.manager._toUpload();
					}
				}else if(self.curr == 3){
					self.tryChangePage(4);
				}else if(self.curr == 4){
					self.tryFinish();
				}
			}else if(type == 'prev'){
				if(self.curr == 1 || self.curr == 3){
					self.changePage(0);
				}else if(self.curr == 2){
					self.changePage(1);
				}else if(self.curr == 4){
					self.changePage(3);
				}
			}else if(type == 'create01'){
				//选在线Chart
				self.changePage(1);
			}else if(type == 'create02'){
				//创建新chart
				self.changePage(3);
			}else if(type == 'create0'){
				self.tryChangePage(6);
			}
		});

		this.parent.show();
		uiUtil.dialogInc();
		animate.normalScaleIn(this.parent);
		event.trigger(event.events.global.blur);

	},

	hide: function(){
		var self = this;
		if(!this.parent){
			return;
		}
		animate.normalScaleOut(this.parent).then(function(){
			self.parent.remove();
			self.parent = null;
			uiUtil.dialogDec();
			event.unbind(event.events.key.up, 'upload_online');
		});
		event.unlock();
		_inited = false;
	}
};

module.exports = ret;