var $ = window.$;
var jade = require('jade');
var animate = require('../../util/animate');
var event = require('../../core/event');
var util = require('../../util/index');
var uiUtil = require('../../util/ui');
var config = require('../../core/config');
var slide = require('../../core/slide');
var notify = require('../notify/ui');
var sound = require('../audio/sound');
var io = require('../io/ui');
var path = require('path');

var _inited = false;

/**
 * 设置, 入口在launcher
 */

var ret = {
	init: function(){
		if(_inited){
			return;
		}
		event.bind(event.events.key.up, 'config', this);
		event.bind(event.events.dialog.config, 'config', this);
		event.lock('key', 'config');
		_inited = true;
	},

	onEvent: function(e, param){
		if(e == event.events.key.up){
			if(param.key == 27){
				this.hide();
				return event.result.stopAll;
			}
		}else if(e == event.events.dialog.config){
			this.show();
			return event.result.stopAll;
		}
	},

	/**
	 * 有些值是在控件onchange中触发的, 不在这里保存
	 */
	save: function () {
		var proxy = this.parent.find('#config_proxy').val();
		var port = this.parent.find('#config_port').val();
		if(proxy){
			if(proxy.indexOf("http") != 0){
				proxy = "http://"+proxy;
			}
			if(!port){
				port = "80";
			}
			config.update(config.ITEM.PROXY, proxy+":"+port);
		}else{
			config.update(config.ITEM.PROXY, "");
		}
		var playSound = this.parent.find("#config_keysound").prop('checked');
		config.update(config.ITEM.PLAYSOUND, playSound);
		config.update(config.ITEM.MARKER_PATH, $("#config_marker").val());
	},

	show: function(){
		var tmpl = jade.compileFile('./tmpl/config/config.jade');
		var html = tmpl({
			config: config
		});
		$('#dialog_layer').append(html);

		var self = this;
		this.parent = $('#dialog_config');

		if($("#edit_area").hasClass("ubeat")){
			this.parent.addClass("pad");
		}else{
			this.parent.removeClass("pad");
		}

		var sliders = this.parent.find('.g_slide');
		sliders.eq(0).attr('id', 'dialog_cfg_vol');
		//事件
		var id = slide.on({
			parent: sliders.eq(0),
			min: 0,
			max: 100,
			onchange: function (val) {
				val = Math.round(val);
				self.parent.find("#volume_val").html(val);
				config.update(config.ITEM.VOL, val);
				sound.fmod.selectTrack(0);
				sound.fmod.volumeTrack(val);
			}
		});

		//换语言
		$('#lang_type').change(function(e){
			var val = $('#lang_type').val();
			config.update(config.ITEM.LANG, val);
			notify.addNotify({
				msg:'Please restart to take effect'
			});
		}).val(config.getVal(config.ITEM.LANG) || 'en');

		this.parent.on('click','.g_barhead i', function () {
			self.hide();
		});
		this.parent.on('click','.g_click', function(){
			var type = $(this).data('type');
			if(type == 'close'){
				self.save();
				self.hide();
			}else if(type == 'path'){
				io.getAny().then(function(_file) {
					if(!_file || _file.indexOf('.exe') < 0){
						return;
					}
					$('#config_path').val(_file);
					config.update(config.ITEM.MA, _file);
				});
			}else if(type == 'marker_path'){
				io.getDir().then(function(d){
					d = d ? d : "";
					$("#config_marker").val(d + path.sep);
				});
			}
		});

		this.parent.show();
		uiUtil.dialogInc();
		animate.normalScaleIn(this.parent).then(function(){
			//填充初始值
			slide.setVal(id, config.getVal(config.ITEM.VOL) || 30, true);
			$('#config_path').val(config.getMaPath() || '');
			$("#config_marker").val(config.getVal(config.ITEM.MARKER_PATH) || "");
			$('#config_keysound').prop('checked', config.getVal(config.ITEM.PLAYSOUND || false));
			var proxy = config.getVal(config.ITEM.PROXY);
			if(proxy){
				var portIdx = proxy.lastIndexOf(":");
				if(portIdx > 7){
					$('#config_proxy').val(proxy.substring(0, portIdx));
					$('#config_port').val(proxy.substring(portIdx + 1));
				}else{
					$('#config_proxy').val(proxy);
					$('#config_port').val("80");
				}
			}
		});
		event.trigger(event.events.global.blur);
	},

	hide: function(){
		var self = this;
		if(!this.parent){
			return;
		}
		animate.normalScaleOut(this.parent).then(function(){
			self.parent.remove();
			self.parent = null;
			uiUtil.dialogDec();
			event.unbind(event.events.key.up, 'about');
			event.unbind(event.events.dialog.about, 'config');
		});
		event.unlock();
		_inited = false;
	}
};

module.exports = ret;