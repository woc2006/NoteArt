var $ = window.$;
var jade = require('jade');
var animate = require('../../util/animate');
var event = require('../../core/event');
var util = require('../../util/index');
var uiUtil = require('../../util/ui');
var config = require('../../core/config');

var _inited = false;

/**
 * 关于, 入口在launcher
 */

var ret = {
	init: function(){
		if(_inited){
			return;
		}
		event.bind(event.events.key.up, 'about', this);
		event.bind(event.events.dialog.about, 'about', this);
		_inited = true;
	},

	onEvent: function(e, param){
		if(e == event.events.key.up){
			if(param.key == 27){
				this.hide();
				return event.result.stopAll;
			}
		}else if(e == event.events.dialog.about){
			this.show();
			return event.result.stopAll;
		}
	},

	show: function(){
		var tmpl = jade.compileFile('./tmpl/config/about.jade');
		var html = tmpl({
			config: config
		});
		$('#dialog_layer').append(html);

		var self = this;
		this.parent = $('#dialog_about');
		this.parent.on('click','.g_barhead i', function () {
			self.hide();
		});

		this.parent.show();
		uiUtil.dialogInc();
		animate.normalScaleIn(this.parent);
		event.trigger(event.events.global.blur);
	},

	hide: function(){
		var self = this;
		if(!this.parent){
			return;
		}
		animate.normalScaleOut(this.parent).then(function(){
			self.parent.remove();
			self.parent = null;
			uiUtil.dialogDec();
			event.unbind(event.events.key.up, 'about');
		});
		_inited = false;
	}
};

module.exports = ret;