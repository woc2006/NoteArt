var $ = window.$;
var event = require('../../core/event');
var util = require('../../util/index');
var Log = require('../../util/logger');
var ui = require('./ui');
var note = require('../note/manager');
var io = require('../io/ui');
var notify = require('../notify/ui');
var config = require('../../core/config');
var path = require('path');

var _init = false;

var ret = {
	init: function(){
		if(_init){
			return;
		}
		event.bind(event.events.dialog.meta, 'meta', this);
		event.bind(event.events.dialog.newmeta, 'meta', this);
		Log.log('meta init');
		_init = true;
	},

	onEvent: function(e, param) {
		if(e == event.events.dialog.meta){
			var meta = this._getMeta();
			if(!meta){
				notify.addNotify({
					msg: config.i18n("err03")
				});
				return true;
			}
			ui.init();
			ui.setManager(this);
			ui.show();
			return true;
		}else if(e == event.events.dialog.newmeta){
			ui.init();
			ui.setManager(this);
			ui.show(true);
			return true;
		}
	},

	_save: function(isNew){
		var data = ui.getData();
		if(!data){
			return;
		}
		if(isNew){
			var title = [data.artist,"-",data.title,"-",data.version,".mce"].join('');
			title = util.filterFilename(title);
			io.getSavePath({name: title}).then(function(file){
				if(!file){
					return;
				}
				if(data.background){
					var name = path.basename(data.background);
					util.copyFile(data.background, path.dirname(file) + '/' + name, false, true);
					data.background = name;
				}
				note.createNew(data, file);
				note.save();
                ui.confirmGuide();
			});
		}else{
			if(data.background){
				note.importFile(data.background, true);
			}
			data.background = path.basename(data.background || "");
			var chart = note.getNote("main");
			var bgChanged = false;
			if(chart.meta.background != data.background){
				bgChanged = true;
			}
			note.updateMeta(chart, data);
			event.trigger(event.events.note.change);
			if(bgChanged){
				var menu = require('../menu/ui');
				menu.checkBackground();
			}
            var mode = chart.meta.mode;
            if(mode == 0||mode == 1||mode == 2){
                var grid = $("#grid");
                var toggle = chart.extra.toggle;
                //这里只要数够有多少个note轨道就可以了 不够了加进去 多了就删
                var track = grid.children("div").first();
                var trackLen = grid.children(".note").length;
                var n = Math.abs(data.column - trackLen);
                var i = 0;
                while(n > 0 && track.length > 0){
                    if(trackLen < data.column){
                        if(!track.hasClass("note") && !track.hasClass("sound")){
                            track.addClass("note");
                            toggle[i] = 1;
                            n --;
                        }
                    }else{
                        if(track.hasClass("note")){
                            track.removeClass("note");
                            toggle[i] = 0;
                            n --;
                        }
                    }
                    i++;
                    track = track.next("div");
                }
            }
		}

	},

	_getMeta: function(){
		var chart = note.getNote('main');
		if(!chart){
			return;
		}
		return chart.meta;
	},

	_getPath: function (name) {
		return note.getFilePath(name);
	}
};

module.exports = ret;
