var $ = window.$;
var jade = require('jade');
var animate = require('../../util/animate');
var event = require('../../core/event');
var uiUtil = require('../../util/ui');

var _queue = [];
var _actionLabel = {
	edit: $('#action_edit')
};

/**
 * 界面各种通知，状态变更的ui管理器
 */

var ret = {
	/**
	 * push一条通知
	 * @param conf
	 */
	addNotify: function(conf){
		var html = ["<div class='item'>",conf.msg,"</div>"].join('');
		var item = $(html);
		var parent = $('#notify_layer');

		if(conf.err){
			item.addClass('err');
		}
		//藏到右边
		item.css({
			'-webkit-transform':'translate(230px, 0)'
		});
		parent.prepend(item);
		item.transition({
			'-webkit-transform':'translate(-10px, 0)'
		}, 640, 'in');

		//把旧item往下推
		for(var i=0;i<_queue.length;i++){
			_queue[i].transition({
				x:'-10',
				y:'+=10'
			}, 640);
		}
		_queue.push(item);
		setTimeout(function(){
			animate.fadeOut(item).then(function(){
				var idx = _queue.indexOf(item);
				if(idx >= 0){
					_queue.splice(idx, 1);
				}
				item.remove();
			});
		}, conf.time || 3000);
	},

	/**
	 * 控制各个地方的状态显示
	 * @param act
	 * @param str
	 */
	changeActionDisplay: function(act, str){
		if(_actionLabel[act]){
			_actionLabel[act].html(str);
		}
	},

	showError: function(){
		if(this.hasError){
			return;
		}
		var self = this;
		var box = $('#error_layer');

		this.hasError = true;
		box.show();
		animate.fadeIn(box, 200).then(function(){
			return animate.fadeOut(box, 450);
		}).then(function(){
			box.hide();
			self.hasError = false;
		});
	},

	showWaiting: function(show) {
		if(show){
			uiUtil.dialogInc();
			$("#g_loading").show();
		}else{
			uiUtil.dialogDec();
			$("#g_loading").hide();
		}
	}
};

module.exports = ret;