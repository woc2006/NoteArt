var $ = window.$;

/**
 * 这不是一个模块, 只是用来启动时给各个模块设定合适尺寸
 * 配合css
 */

module.exports = {
	resize: function(){
		var body = $('body');
		var w = body.width();
		var h = body.height();
		console.log('size:'+w+'/'+h);
		//主区域高度
		body.find('#body').css('height', (h-66)+'px');

		h -= 66;

		w = $('#edit').width();

		$('#timeline').css('height', '40px');
		$('#edit_up').css('height', (h - 40)+'px');
		//$('#edit_area').css('height', (h-40 - 16)+'px'); //减掉下面toggle的高度

		//$('#note_bar').css('width', (w - 64*13 - 4)+'px');
		//进度条
		var _w = w - 76 - 6 - 120;
		$("#timeline_area").css('width', (_w)+'px');
		$('#timeline_bar').css('width', (_w - 14)+'px');
	}
};