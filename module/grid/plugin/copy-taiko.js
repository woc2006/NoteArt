var $ = window.$;
var frac = require('../../../util/frac');
var event = require('../../../core/event');
var sound = require('../../audio/sound');
var sample = require('../../sample/manager');
var timing = require('../../timing/manager');
var Log = require('../../../util/logger');


var ret = {
	setUI: function (_ui) {
		this.ui = _ui;
	},

	onCancel: function(){
		
	},

	onCopy: function(){
		var notes = this.ui.selectedNotes;
		var tmp = [];
		for(var k in notes){
			tmp.push(this.ui.manager.getNote(k));
		}
		if(tmp.length){
			this.select = tmp;
			Log.i("copy " + tmp.length + " notes");
		}
	},

	onPaste: function () {
		if(!this.select){
			Log.i("paste note: 0!!");
			return;
		}
		var now = timing.timeToBeat(sound.stableTime(), this.ui.divide);
		var firstBeat = this.select[0].beat;
		for(var i=0;i<this.select.length;i++){
			var note = $.extend({}, this.select[i]);
			delete note.id;
			if(note.sound){
				note.tid = -1;
			}
			note.beat = frac.add(now, frac.subtract(note.beat, firstBeat));
			if(note.endbeat){
				note.endbeat = frac.add(now, frac.subtract(endbeat, firstBeat));
			}
			this.ui.pluginTaikoEdit.makeNewNote(note);
		}
		this.ui.fullUpdateNotes();
		event.trigger(event.events.note.change);
	},

	onCut: function(){
		if(!this.select){
			return;
		}

		event.trigger(event.events.note.change);
	}
};

module.exports = ret;