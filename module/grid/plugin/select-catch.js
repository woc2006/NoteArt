var frac = require('../../../util/frac');
var vector = require('../vector');
var sound = require('../../audio/sound');
var timing = require('../../timing/manager');
var event = require('../../../core/event');
var input = require('../../../core/input');

/**
 * 非独立模块, 作为ui的插件
 */


//
//var ret = {
//	setUI: function (_ui) {
//		this.ui = _ui;
//	},
//
//	/**
//	 * 注释见select.js
//	 * @param pos
//	 * @param note
//	 */
//	onMouseDown: function(pos, note){
//		var gridPos = this.ui.layerToGridOffset(pos);
//		//选择时, 选择区域始终在parent范围内, 但折算的layer区域在变化
//		if(note){
//			//select状态下,如果点中了note, 那么进入移动
//			//var unsnap = this.ui.mouseToGridUnsnap(this.ui.mouseStart);
//			var snap = this.ui.noteToParam(note);
//			gridPos = snap;
//			this.dragStartGrid = gridPos;
//			this.dragCurrGrid = gridPos;
//			this.selectStartGrid = null;
//			//点击的note本身 要算被选中
//			var div = this.ui.getNoteDiv(note.id);
//			this.dragStartNoteDiv = div;  //记录被选中的note
//			if(div.hasClass('curr')){
//				this.dragNoteCurr = true;
//			}else{
//				//div.addClass('curr');  //这里已经当作单选先选中了
//				this.dragNoteCurr = false;
//			}
//			this.dragStart = false;
//
//			//this.dragObjects = this.ui.note.find('.curr');
//		}else{
//			//否则进入框选
//			this.ui.cancelSelect();
//			this.dragStartGrid = null;
//			this.dragStartNoteDiv = null;
//			this.selectStartGrid = this.ui.layerToGridUnsnap(pos);
//			this.ui.select.css({
//				left: this.ui.mouseStart[0],
//				top: this.ui.mouseStart[1],
//				width: '1px',
//				height:'1px'
//			});
//		}
//	},
//
//	onMouseMove: function (pos) {
//		if(this.dragStartGrid){
//			var gridPos = this.ui.mouseToGridOffset(pos);
//			if(!vector.equal(gridPos, this.dragCurrGrid)){
//				if(!this.dragStart){
//					//进行拖动前的处理
//					if(this.dragNoteCurr){
//						this.dragObjects = this.ui.note.find('.curr');
//					}else{
//						this.dragStartNoteDiv.addClass('curr');
//						this.dragObjects = this.dragStartNoteDiv;
//					}
//					this.dragStart = true;
//                    this.ui.pluginUndo.mark("drag_start");
//				}
//				var deltaY = this.ui.gridToLayerY(gridPos) - this.ui.gridToLayerY(this.dragCurrGrid);  //y差值
//				var deltaX = (gridPos[3] - this.dragCurrGrid[3]) * this.ui.scaleW;
//				this.dragCurrGrid = gridPos;
//				for(var i=0;i<this.dragObjects.size();i++){
//					var obj = this.dragObjects.eq(i);
//					var offset = this.ui.getDivOffset(obj);
//					obj.css({
//						bottom: offset.bottom + deltaY,
//						left: offset.left + deltaX
//					});
//				}
//				if(this.dragObjects.size() == 1){
//					this.checkSelectTip(this.dragObjects);
//				}
//			}
//		}else{
//			var size = vector.subAbs(pos, this.ui.mouseStart);
//			var conf = {
//				width: size[0],
//				height: size[1]
//			};
//			if(pos[0] < this.ui.mouseStart[0]){
//				conf.left = pos[0];
//			}else{
//				conf.left = this.ui.mouseStart[0];
//			}
//			if(pos[1] < this.ui.mouseStart[1]){
//				conf.top = pos[1];
//			}else{
//				conf.top = this.ui.mouseStart[1];
//			}
//			this.ui.select.css(conf);
//
//			if(size[0] > 6 || size[1] > 6){
//				this.ui.select.show();  //产生一定移动才显示选择框
//			}
//		}
//	},
//
//	onMouseUp: function(pos){
//		var move = vector.subAbs(pos, this.ui.mouseStart);
//		var noMove = move[0] < 6 && move[1] < 6;
//		if(noMove){
//			if(this.dragStartNoteDiv){
//				if(this.dragNoteCurr){
//					if(input.isCtrl()){
//						this.dragStartNoteDiv.removeClass('curr');
//					}else{
//						this.ui.cancelSelect();
//						this.dragStartNoteDiv.addClass('curr');
//					}
//				}else{
//					//按CTRL时添加note是多选, 否则先清空
//					if(!input.isCtrl()){
//						this.ui.cancelSelect();
//					}
//					this.dragStartNoteDiv.addClass('curr');
//				}
//			}else if(this.selectStartGrid){
//				this.ui.cancelSelect();
//			}
//            this.ui.pluginUndo.mark("drag_cancel");
//			return;
//		}
//		if(this.selectStartGrid){
//			this.ui.select.hide();
//			//处理选中note
//			var endGrid = this.ui.mouseToGridUnsnap(pos);
//			var hasEdge = pos[0] < this.ui.mouseStart[0]; //从右向左拉, 要包含跨边
//			var _startY = this.ui.gridToLayerY(this.selectStartGrid);
//			var _endY = this.ui.gridToLayerY(endGrid);
//			//转换到左下 - 右上layer坐标
//			//x可以用mouse值, 但y值必须从grid值转回layer值
//			this.rectSelect([
//				Math.min(this.ui.mouseStart[0], pos[0]),
//				Math.min(_startY, _endY)
//			],[
//				Math.max(this.ui.mouseStart[0], pos[0]),
//				Math.max(_startY, _endY)
//			], hasEdge);
//			this.checkSelectTip();
//		}else if(this.dragStartGrid){
//			var gridPos = this.ui.mouseToGridOffset(pos);
//			var delta = frac.subtract(gridPos, this.dragStartGrid);
//			var deltaX = gridPos[3] - this.dragStartGrid[3];
//			//拖动模式的最后, 要移除编辑区域以外的note
//			for(var i =0;i<this.dragObjects.size();i++){
//				var obj = this.dragObjects.eq(i);
//				var offset = this.ui.getDivOffset(obj);
//
//				if((offset.left >= this.ui.totalW) ||
//					(offset.left <= 0) ||
//					(offset.bottom < 0)){
//					this.ui.manager.removeNote(+obj.data('tag'));
//					obj.remove();
//					continue;
//				}
//
//				//最后修正note的grid值
//				var note = this.ui.manager.getNote(+obj.data("tag"));
//				note.beat = frac.add(note.beat, delta);
//				note.x += deltaX;
//
//				var time = timing.beatToTime(note.beat) + (note.offset || 0);
//				if (note.tid >= 0) {
//					sound.offsetCell(note.tid, time);
//				}
//				this.ui.checkMaxTime(time);
//
//			}
//			this.checkSelectTip();
//			event.trigger(event.events.note.change);
//            this.ui.pluginUndo.mark("drag_end");
//			this.dragObjects = null;
//		}
//	},
//
//	rectSelect: function(leftBottom, rightTop, hasEdge){
//		var notes = this.ui.note.find('div');
//		var len = notes.size();
//		for(var i=0;i<len;i++){
//			var note = notes.eq(i);
//			var rect = note[0].getBoundingClientRect();
//			var left = parseFloat(note.css('left')) - rect.width / 2;
//			var bottom = parseFloat(note.css('bottom')) - rect.height / 2;
//			if(left + rect.width <= leftBottom[0]){
//				continue;
//			}else if(!hasEdge && left < leftBottom[0]){
//				continue;
//			}
//			if(left >= rightTop[0]){
//				continue;
//			}else if(!hasEdge && left + rect.width > rightTop[0]){
//				continue;
//			}
//			if(bottom + rect.height <= leftBottom[1]){
//				continue;
//			}else if(!hasEdge && bottom < leftBottom[1]){
//				continue;
//			}
//			if(bottom >= rightTop[1]){
//				continue;
//			}else if(!hasEdge && bottom + rect.height > rightTop[1]){
//				continue;
//			}
//			note.addClass('curr');
//		}
//	},
//
//	/**
//	 * 检查是否只有2个note被选中
//	 * fixme 计数方式有待优化
//	 */
//	checkSelectTip: function(notes) {
//		if(!notes){
//			notes = this.ui.note.find('.curr');
//		}
//		this.ui.vert.hide();
//		this.ui.range.hide();
//
//		if(notes.size() == 1){
//			var left = parseFloat(notes.css('left'));
//			this.ui.vert.css('left', left).show();
//		}else if(notes.size() == 2){
//			var note1 = notes.eq(0);
//			var note2 = notes.eq(1);
//			//bottom较小那个为主note
//			var bottom1 = parseFloat(note1.css('bottom'));
//			var bottom2 = parseFloat(note2.css('bottom'));
//			var left = 0;
//			if(bottom1 < bottom2){
//				left = parseFloat(note1.css('left'));
//			}else{
//				left = parseFloat(note2.css('left'));
//			}
//			this.ui.range.css('left', left);
//			var _note1 = this.ui.manager.getNote(+note1.data('tag'));
//			var _note2 = this.ui.manager.getNote(+note2.data('tag'));
//			var time1 = timing.beatToTime(_note1.beat);
//			var time2 = timing.beatToTime(_note2.beat);
//
//			var delta = Math.abs(time1 - time2);
//			//可能时间点重合了, 或者mapper该吃药
//			if(delta < 10){
//				return;
//			}
//			//t时间能可运动的范围等于t像素 * grid缩放
//			//实际范围还要加上半个托盘宽度
//			//dash范围是normal的2倍
//			this.ui.updateRange(delta * this.ui.scaleW);
//			this.ui.range.show();
//		}
//	}
//};

module.exports = GridEdit;