var $ = window.$;
var input = require('../../../core/input');
var sample = require('../../sample/manager');
var frac = require('../../../util/frac');
var layer = require('../../layer/manager');
var sound = require('../../audio/sound');
var timing = require('../../timing/manager');
var record = require('../../../core/record');
var util = require('../../../util/index');
var config = require('../../../core/config');
var vector = require('../vector');
var event = require('../../../core/event');
var slide = require('../../../core/slide');

/**
 * 这个类控制右侧的属性面板
 * 这个面板不属于grid, 但跟grid紧密相关, 所以这里
 *
 * todo: 把所有操纵selected notes的收归到这里
 * @constructor
 */

var GridMeta = function(){

};

GridMeta.prototype = {
	init: function() {
		this.parent = $("#note_meta");
		this.tip = {
			number: this.parent.find("#note_meta_num"),
			sample: this.parent.find("#note_meta_sam"),
			layer: this.parent.find("#note_meta_layer"),
			volume: this.parent.find("#note_meta_vol")
		};

		var self = this;
		this.targets = [];
		var sliders = this.parent.find('.g_slide');
		this.slideId = [];

		var id = slide.on({
			parent: sliders.eq(0),
			min: 1,
			max: 100,
			onchange: function(val){
				var _val = Math.round(val);
				self.tip.volume.html(_val);
				self.updateTargets('vol', _val);
			}
		});
		this.slideId.push(id);

		this.parent.on('click', '.tool', function(e) {
			var type = $(this).data("type");
			var toggle = require('../../toggle/ui');
			toggle.changeList(type);
		});

		this.parent.on('click','.g_click', function(e){
			event.trigger(event.events.sample.edit, self.targets);
		});

		event.bind(event.events.grid.assign, 'grid_note_meta', this);
	},

	setUI: function(ui) {
		this.ui = ui;
	},

	release: function() {
		this.clear();
		slide.off(this.slideId[0]);
		event.unbindByKey('grid_note_meta');
	},

	onEvent: function(tag, data){
		if(tag == event.events.grid.assign){
			if(!data){
				return false;
			}
			if(!this.targets.length){
				return false;
			}
			if(typeof data.color != 'undefined'){
				this.updateTargets('color', data.color);
			}
			if(typeof data.sample != 'undefined'){
				this.updateTargets('sound', data.sample);
			}
			this.update(this.targets);
		}
		return false;
	},

	updateTargets: function(key, val){
		if(val === null){
			for(var i=0;i<this.targets.length;i++){
				var note = this.targets[i];

				delete note[key];
				//属性关联
				if(key == 'sound' && note.tid >= 0){
					sound.removeCell(note.tid);
					delete note.tid;
					delete note.vol;
				}
			}
		}else{
			for(var i=0;i<this.targets.length;i++){
				var note = this.targets[i];
				note[key] = val;

				if(key == 'vol' && note.tid >= 0){
					sound.volumeCell(note.tid, val);
				}
				if(key == 'sound'){
					if(note.tid >= 0){
						sound.removeCell(note.tid);
						note.tid = -1;
					}

					var time = timing.beatToTime(note.beat) + (note.offset || 0);
					note.tid = sound.addCell(sample.getFilePath(note.sound), note.vol || 30, time);
				}
			}
		}
	},

	update: function(notes) {
		if(!notes){
			notes = this.ui.manager.getSelectNotes({allMode: true});
		}
		if(notes.length == 0){
			this.clear();
			return;
		}
		this.targets = notes;
		this.tip.number.html(notes.length);

		var sample = null;
		var layer = null;
		var volume = null;
		for(var i=0;i<notes.length;i++){
			var note = notes[i];
			if(note.sound && !sample){
				sample = note.sound;
			}
			if(note.sound != sample){
				sample = -1;
			}
			if(typeof note.color == 'number' && !layer){
				layer = note.color;
			}
			if(note.color != layer){
				layer = -1;
			}
			if(typeof note.vol =='number' && !volume){
				volume = note.vol;
			}
			if(note.vol != volume){
				volume = -1;
			}
			if(sample === -1 && layer === -1 && volume === -1){
				break;
			}
		}
		if(sample === null){
			this.tip.sample.html("None");
		}else if(sample === -1){
			this.tip.sample.html("Multi");
		}else{
			this.tip.sample.html(sample);
		}

		if(layer === null){
			this.tip.layer.html("None");
		}else if(layer === -1){
			this.tip.layer.html("Multi");
		}else{
			var _layer = require('../../layer/manager');
			var layerObj = _layer.getLayer(layer);
			if(layerObj && layerObj.name){
				this.tip.layer.html(layerObj.name);
			}else{
				this.tip.layer.html("Assigned");
			}
		}

		if(volume === null || volume == -1){
			slide.setVal(this.slideId[0], 30, false);
			this.tip.volume.html("30");
		}else{
			slide.setVal(this.slideId[0], volume, false);
			this.tip.volume.html(volume);
		}
	},

	clear: function() {
		this.tip.number.html("0");
		this.tip.sample.html("None");
		this.tip.layer.html("None");
		this.tip.volume.html("");
		this.targets = [];
	}
};

module.exports = GridMeta;