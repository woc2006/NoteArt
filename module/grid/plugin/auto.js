var $ = window.$;
var input = require('../../../core/input');
var frac = require('../../../util/frac');
var sound = require('../../audio/sound');
var timing = require('../../timing/manager');
var util = require('../../../util/index');
var config = require('../../../core/config');
var vector = require('../vector');
var event = require('../../../core/event');
var notify = require('../../notify/ui');

var ret = {
	init: function(){

	},

	setManager: function(ma){
		this.manager = ma;
	},

	//对谱面简化,以适应触屏
	//规则:一排最大2个note
	//有hold的情况, 优先保留hold
	//有多个hold的情况, 在2note已满的情况, 抛弃后加入的hold
	//单note优先分布在12 + 34 两边, 分布情况随机
	toTouch: function(chart){
		var row = new Array(10);
		var column = util.getJsonChainValue(chart, "meta.mode_ext.column") || 4;
		var notes = chart.note;
		var newArr = [];
		var curr = [0,0,1];

		var pushNotes = function(curr){
			for(var col = 0;col < column;col++){
				if(row[col]){
					var note = row[col];
					if(!note.endbeat){
						newArr.push(note);
						row[col] = null;
					}else if(frac.compare(note.endbeat, curr) <= 0){
						newArr.push(note);
						row[col] = null;
					}
				}
			}
		};

		for(var i=0;i<notes.length;i++){
			var note = notes[i];
			if(note.type == 1){
				newArr.push(note);
				continue;
			}
			if(frac.compare(curr, note.beat) != 0){
				//新beat, 处理上一批
				var total = 0;
				for(var col = 0;col < column;col++){
					if(row[col]){
						total++;
					}
				}
				if(total <= 2){
					pushNotes(note.beat);
				}else{
					total = 0;
					var temp = new Array(10);
					//优先留hold
					for(var col = 0;col < column;col++){
						if(row[col] && row[col].endbeat){
							temp[col] = row[col];
							total++;
							if(total == 2){
								break;
							}
						}
					}
					if(total < 2){
						//普通note还有机会
						for(var col = 0;col < column;col++){
							if(row[col] && !row[col].endbeat){
								temp[col] = row[col];
								total++;
								if(total == 2){
									break;
								}
							}
						}
					}
					//对于抛弃的note, 考虑一下audio cell
					for(var col = 0;col < column;col++){
						if(row[col] && !temp[col] && row[col].tid){
							sound.removeCell(note.tid);
						}
					}
					//再考虑一下位置
					var pos = [];
					var half = Math.floor(column / 2);
					for(var col = 0;col<column;col++){
						if(temp[col]){
							pos.push(col);
						}
					}
					if(pos[0] < half && pos[1] < half){
						if(half % 2 == 0){
							temp[half + Math.floor(Math.random()*half)] = temp[pos[1]];
						}else{
							temp[half + Math.floor(Math.random()*(half+1))] = temp[pos[1]];
						}
						temp[pos[1]] = null;
					}else if(pos[0] >= half && pos[1] >= half){
						temp[Math.floor(Math.random()*half)] = temp[pos[0]];
						temp[pos[0]] = null;
					}

					row = temp;
					pushNotes(note.beat);
				}

				curr = note.beat;
			}

			if(row[note.column]){
				//重叠了
				notify.addNotify({
					msg: config.i18n("err28") + " ["+curr[0]+":"+curr[1]+"/"+curr[2]+"]",
					err: true
				});
				return;
			}
			row[note.column] = note;
		}

		chart.note = newArr;
	}
};

module.exports = ret;