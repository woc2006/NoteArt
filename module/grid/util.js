exports.clampDivide = function(val, up){
	val = Math.round(val);
	if(val < 2){
		val = 2;
	}else if(val == 5){
 		val = up ? 6 : 4;
	}else if(val == 7){
		val = up ? 8: 6;
	}else if(val > 8 && val < 12){
		val = up ? 12 : 8;
	}else if(val > 12 && val < 16){
		val = up? 16 : 12;
	}else if(val > 16){
		val = 16;
	}
	return val;
};