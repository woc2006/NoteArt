exports.sub = function(v0, v1){
	return [v0[0] - v1[0], v0[1] - v1[1]];
};

exports.subAbs = function(v0, v1){
	return [Math.abs(v0[0] - v1[0]), Math.abs(v0[1] - v1[1])];
};

exports.distance = function (v) {
	return Math.sqrt(Math.pow(v[0], 2) + Math.pow(v[1], 2));
};

exports.equal = function(v0, v1){
	if(!v0 || !v1){
		return false;
	}
	for(var i=0;i<v0.length;i++){
		if(v0[i] != v1[i]){
			return false;
		}
	}
	return true;
};