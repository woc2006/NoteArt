var $ = window.$;
var jade = require('jade');
var animate = require('../../../util/animate');
var event = require('../../../core/event');
var util = require('../../../util/index');
var uiUtil = require('../../../util/ui');
var config = require('../../../core/config');
var slide = require('../../../core/slide');
var gridUtil = require('../util');
var Log = require('../../../util/logger');

var _inited = false;

/**
 * grid设定
 */

var ret = {
	init: function(){
		if(_inited){
			return;
		}
		this.slideId = [];
		event.bind(event.events.key.up, 'grid_config', this);
		event.lock('key', 'grid_config');
		_inited = true;
	},

	setManager: function (ma) {
		this.manager = ma;
	},

	onEvent: function(e, param){
		if(param.key == 27){
			this.hide();
			return event.result.stopAll;
		}
	},

	save: function () {
		var divide = this.parent.find("#dialog_grid_divide").val();
		var zoom = this.parent.find("#dialog_grid_zoom").val();
		divide = Math.round(parseInt(divide));
		zoom = parseFloat(zoom);

		divide = gridUtil.clampDivide(divide);
		zoom = util.clamp(zoom, 1, 3);

		var info = this.manager.getEditInfo();
		if(!info.grid){
			info.grid = {};
		}
		config.update(config.ITEM.GRID_DIVI, divide);
		config.update(config.ITEM.GRID_ZOOM, zoom);

		//额外部分
		if(this.mode == 3){
			var vert = this.parent.find("#dialog_grid_vert").val();
			vert = parseInt(vert);
			vert = util.clamp(vert, 1, 50);
			info.grid.vertical = vert;
		}else if(this.mode ==4){
            //pad mode do nothing
        }else{
			//轨道状态
			var toggle = info.toggle;
            var column = 0;
			for(var i=0;i<toggle.length;i++){
				var btn = this.trackStatus.find("[track-index=" + i + "]");
				var tog = +btn.data("tog");
				toggle[i] = tog;
                if(tog == 1){
                    column ++;
                }
			}
            var meta = this.manager.chart.meta;
            if(meta.mode_ext){
                meta.mode_ext.column = column;
            }
		}

		event.trigger(event.events.note.change);
		event.trigger(event.events.grid.change, info.grid);
	},

	show: function(param){
		//参数准备, param外部已检验
		this.param = param;
		this.mode = param.meta.mode;
		var tmpl = jade.compileFile('./tmpl/grid/dialog.jade');
		var html = tmpl({config: config});
		$('#dialog_layer').append(html);

		var self = this;
		this.parent = $('#dialog_grid');
        this.trackStatus = $("#dialog_grid_track_stat");

		if(this.mode == 3){
			this.parent.addClass('catch');
            this.trackStatus.hide();
		}else if(this.mode == 4){
            this.parent.addClass("pad");
        }else{
			this.trackStatus.show();
			var info = this.manager.getEditInfo();
			var toggle = info.toggle;
			for(var i=0;i<toggle.length;i++){
				var btn = this.trackStatus.find("[track-index=" + i + "]");
				var stat = btn.children(".stat");
				btn.data("tog", toggle[i]);
				if(toggle[i] == 1){
					stat.html(config.i18n("grid07"));
					//btn.removeClass("sound");
				}else if(toggle[i] == 2){
					stat.html(config.i18n("grid08"));
					//btn.addClass("sound");
				}else{
					stat.html(config.i18n("grid11"));
				}
			}
		}


		this.parent.on('click','.g_barhead i', function () {
			self.hide();
		});
		this.parent.on('click','.g_click', function(){
            var btn = $(this);
			var type = btn.data('type');
			if(type == 'close'){
				self.save();
				self.hide();
			}
            if(btn.attr("track-index") !== undefined){
				var tog = +btn.data("tog");
				if(tog == 0){
					tog = 1;
					btn.children(".stat").html(config.i18n("grid07"));
				}else if(tog == 1){
					tog = 2;
					btn.children(".stat").html(config.i18n("grid08"));
				}else{
					tog = 0;
					btn.children(".stat").html(config.i18n("grid11"));
				}
				btn.data("tog", tog);
            }
		});

		var sliders = this.parent.find('.g_slide');
		var inputs = this.parent.find('input');

		var id = slide.on({
			parent: sliders.eq(0),
			min: 2,
			max: 16,
			onchange: function(val){
				inputs.eq(0).val(gridUtil.clampDivide(val));
			}
		});
		this.slideId.push(id);
		id = slide.on({
			parent: sliders.eq(1),
			min: 1,
			max: 3,
			onchange: function(val){
				inputs.eq(1).val(val.toFixed(1));
			}
		});
		this.slideId.push(id);

		if(this.mode == 3){
			id = slide.on({
				parent: sliders.eq(2),
				min: 1,
				max: 50,
				onchange: function(val){
					inputs.eq(2).val(val.toFixed(0));
				}
			});
			this.slideId.push(id);
		}

		inputs.keyup(function (e) {
			var target = $(this);
			var id = target.attr('id');
			if(id == 'dialog_grid_divide'){
				slide.setVal(self.slideId[0], gridUtil.clampDivide(parseInt(target.val())));
			}else if(id == 'dialog_grid_zoom'){
				slide.setVal(self.slideId[1], parseInt(target.val()));
			}else if(id == 'dialog_grid_vert'){
				slide.setVal(self.slideId[2], parseInt(target.val()));
			}
		});

		this.parent.show();
		uiUtil.dialogInc();
		animate.normalScaleIn(this.parent).then(function(){
			var info = self.manager.getEditInfo();
			if(!info.grid){
				info.grid = {};
			}
			slide.setVal(self.slideId[0], config.getVal(config.ITEM.GRID_DIVI) || 4, true);
			slide.setVal(self.slideId[1], config.getVal(config.ITEM.GRID_ZOOM) || 1, true);
			//inputs.eq(0).val(info.grid.divide || 4);
			//inputs.eq(1).val(info.grid.zoom || 1);
			if(self.mode == 3){
				slide.setVal(self.slideId[2], info.grid.vertical || 1);
				inputs.eq(2).val(info.grid.vertical || 1);
			}
		});
		event.trigger(event.events.global.blur);

	},

	hide: function(){
		var self = this;
		if(!this.parent){
			return;
		}
		animate.normalScaleOut(this.parent).then(function(){
			self.parent.remove();
			self.parent = null;
			uiUtil.dialogDec();
			event.unbind(event.events.key.up, 'grid_config');
			for(var i=0;i<self.slideId.length;i++){
				slide.off(self.slideId[i]);
			}
		});
		event.unlock();
		_inited = false;
	}
};

module.exports = ret;