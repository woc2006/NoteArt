var $ = window.$;
var jade = require('jade');
var animate = require('../../../util/animate');
var event = require('../../../core/event');
var util = require('../../../util/index');
var uiUtil = require('../../../util/ui');
var config = require('../../../core/config');


var _inited = false;

/**
 * grid设定
 */

var ret = {
    init: function(){
        if(_inited){
            return;
        }
        event.lock('key', 'min_interval');
        _inited = true;
    },

    setManager: function (ma) {
        this.manager = ma;
    },

    onEvent: function(e, param){

    },


    show: function(param){
        //参数准备, param外部已检验
        this.param = param;
        var tmpl = jade.compileFile('./tmpl/grid/min_interval.jade');
        var html = tmpl({config: config});
        $('#dialog_layer').append(html);

        var self = this;
        this.parent = $('#dialog_interval');
        this.parent.on('click','.g_barhead i', function () {
            self.hide();
        });
        this.parent.on('click','.g_click', function(){
            var btn = $(this);
            var type = btn.data('type');
            if(type == 'close'){
                var val = +$("#dialog_min_interval").val();
                if(val <= 833){
                    val = 833;
                }
                util.setJsonChainValue(self.manager.chart, "extra.min_interval", val);
                self.hide();
            }
        });

        this.parent.show();
        uiUtil.dialogInc();
        animate.normalScaleIn(this.parent).then(function(){
            var chart = self.manager.chart;
            var val = chart.extra && chart.extra.min_interval ? chart.extra.min_interval : 1033;
            $("#dialog_min_interval").val(val);
        });
        event.trigger(event.events.global.blur);

    },

    hide: function(){
        var self = this;
        if(!this.parent){
            return;
        }
        animate.normalScaleOut(this.parent).then(function(){
            self.parent.remove();
            self.parent = null;
            uiUtil.dialogDec();
            event.trigger(event.events.grid.interval_change);
        });
        event.unlock();
        _inited = false;
    }
};

module.exports = ret;