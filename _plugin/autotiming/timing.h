#pragma once

#include <vector>


// 音频滤波
std::vector<float> preprocess(const std::vector<float> & audioData, unsigned sampleRate);

/*
 * 返回值：
 * 0		正常
 * 负		算不出
 * 正		精度低
 * 抛异常	出错
 */
int calcBpm(const std::vector<float> & feature, double & bpm, double & uncertainty, unsigned & signature, unsigned & division);
int calcOffset(const std::vector<float> & feature, double bpm, double & offset);

double snapBpm(double bpm, double uncertainty);
