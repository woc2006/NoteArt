#ifndef __INCLUDE_MACRO_H__
#define __INCLUDE_MACRO_H__

#define CC_PLATFORM_WIN32 1
#define CC_PLATFORM_MAC 2
#define CC_PLATFORM_IOS 0

#define CC_TARGET_PLATFORM CC_PLATFORM_WIN32

inline float getNaN() { unsigned int i = 0x7fc00000; float f; memcpy(&f, &i, 4); return f; }
#define NAN (getNaN())

#endif