//
//  LogUtil.h
//  mania
//
//  Created by dolly on 14-4-2.
//
//

#ifndef __mania__LogUtil__
#define __mania__LogUtil__

#include <iostream>
#include <fstream>
#include <mutex>
#include <atomic>
#include "TimeUtil.h"


class LogUtil{
private:
	static std::mutex mtx;
    static std::atomic<bool> allowDebugLog;
    static std::atomic<bool> allowTimerLog;
    std::ostream * logFile;
    MMonotonicClockType lastTime;
    static std::atomic<LogUtil *> instance;
    explicit LogUtil(void);
	~LogUtil(void);
    void writeLog(const char * line, bool flush);
    void vaWriteLog(const char * tag, const char * type, const char * format, va_list ap, bool flush);
public:
    static LogUtil* getInstance();
    static void releaseInstance();
    static void setAllowDebugLog(bool allow); //是否允许输出调试log
    static void setAllowTimerLog(bool allow); //是否允许输出计时log
    LogUtil* flush();
    LogUtil* logDontFlush(const char * tag, const char * format, ...);
    LogUtil* log(const char * tag, const char * format, ...);
    LogUtil* debug(const char * tag, const char * format, ...);
    LogUtil* error(const char * tag, const char * format, ...);
    //计时器，如果允许timer log，调end时会在log里输出start到end之间的用时
    //如果连续调用两次end，也会输出两次end之间的用时，但包含第一次end输出log本身的时间，不推荐
    LogUtil* timerStart();
    LogUtil* timerEnd(const char * tag, const char * msg);
};

#endif /* defined(__mania__LogUtil__) */
