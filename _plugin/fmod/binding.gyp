{
  'targets': [
    {
      'target_name': 'fmod',
      'sources': [ 'binding.cc' ,'AudioManager.cpp', 'GlobalFunc.cpp', 'LogUtil.cpp', 'TimeUtil.cpp'],
      "cflags_cc!": [ "-fno-rtti", "-fno-exceptions" ],
	  "cflags!": [ "-fno-exceptions" ],
	  'conditions':[
			['OS=="win"',{
				'msvs_settings': {
					'VCCLCompilerTool': {
						'DisableSpecificWarnings': ['4819']
					}
				},
				'include_dirs':['../public/win32/fmod/inc', '../public/win32'],
				'libraries':['../../public/win32/fmod/lib/fmod_vc.lib']
			}],
			['OS=="mac"',{
				"xcode_settings": {
                    'OTHER_CPLUSPLUSFLAGS' : ['-std=c++11','-stdlib=libc++', '-v'],
                    'OTHER_LDFLAGS': ['-stdlib=libc++'],
                    'MACOSX_DEPLOYMENT_TARGET': '10.9',
                    'GCC_ENABLE_CPP_EXCEPTIONS': 'YES'
                },
				'include_dirs':['../public/mac/fmod/inc', '../public/mac'],
				'libraries':['../../public/mac/fmod/lib/libfmod.dylib']
			}]
	  ]
    }
  ]
}