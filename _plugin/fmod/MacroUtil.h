//  MACRO.h
//  mania
//
//  Created by dolly on 14-3-1.
//
//

#ifndef mania_MACRO_h
#define mania_MACRO_h

#include "GlobalFunc.h"
#include "macro.h"

//函数宏
#define SAFE_COPY_LEN(desc, src, len) \
    do { \
	    if(desc) { \
		    delete[] desc; \
            desc = nullptr; \
        } \
	    desc = new char[(len)+1]; \
	    memset(desc,0, (len)+1); \
	    strncpy(desc, src, len); \
    } while (0)

#define SAFE_COPY(desc, src)\
        do {\
			if(desc) { \
				delete[] desc; \
                desc = nullptr; \
            } \
			if(src != NULL){ \
				size_t len = strlen(src);\
				desc = new char[len+1]; \
				memset(desc,0, len+1); \
				strcpy(desc, src);\
            }\
        } while (0)

#define SAFE_LEN(str) (str == nullptr? 0 : strlen(str))

#define SAFE_DEL(obj) \
		if(obj != nullptr){\
			delete obj; \
			obj = nullptr;\
		}

#define SAFE_DELS(obj)\
		if(obj != nullptr){\
			delete[] obj; \
			obj = nullptr;\
		}

//不要乱用这个宏，参照已使用过的地方，否则玩脱不可避
#define NO_SPACE(src)\
        do {\
            if (src != nullptr) { \
                while(*src && isspace(static_cast<unsigned char>(*src)))\
                    src++;\
            } \
        } while (0)

// Text encoding related
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
# define FOPEN(filename, mode) _wfopen(Widen(filename).c_str(), Widen(mode).c_str())
# define FMOD_CHARSET 0
#else
# define FOPEN(filename, mode) fopen(filename, mode)
# define FMOD_CHARSET 0
#endif

// Global Functions related

#if CC_TARGET_PLATFORM != CC_PLATFORM_WIN32
#define malodyMakeDIR(filename) mkdir(filename,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)
#define mRemoveFile(filename) remove(filename)
#define mRenameFile(oldname, filename) rename(oldname, filename)
#define mRemoveDIR(dirname) rmdir(dirname) 
#else
#define malodyMakeDIR(filename) _wmkdir(Widen(filename).c_str())
#define mRemoveFile(filename) _wremove(Widen(filename).c_str())
#define mRenameFile(oldname,newname) _wrename(Widen(oldname).c_str(),Widen(newname).c_str())
#define mRemoveDIR(dirname) _wrmdir(Widen(dirname).c_str())
#endif

typedef enum
{
	GlobalFlag_Success = 0,
	GlobalFlag_CommonError = -1, //常规错误
	GlobalFlag_IOError = -2,	//无法读取/写入文件或文件夹
    GlobalFlag_NotExist = -3,  //お探しの物はあ～り～ま～せ～ん～
    GlobalFlag_Empty = -4, //结果集为空
	GlobalFlag_BadAccess = -5,	//目标容器、对象或变量等无法访问
	GlobalFlag_BadArgument = -6, //参数错误或传入参数检查未通过
	GlobalFlag_Unknown = -7,	//执行状态未知，需要进一步确认
    GlobalFlag_Cancelled = -8,  //操作已取消
    GlobalFlag_WTF = -10
}GlobalFlag;

enum RotateType{
	RotateType_90 = 1,
	RotateType_180 = 1 << 1,
	RotateType_270 = 1 << 2,
	RotateType_FlipX = 1 << 3,
	RotateType_FlipY = 1 << 4,
    RotateType_Max = 1 << 5
};

#endif





