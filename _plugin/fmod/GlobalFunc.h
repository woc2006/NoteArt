#ifndef __INCLUDE_GLOBALFUNC_H__
#define __INCLUDE_GLOBALFUNC_H__

#include <cstdio>
#include <cstring>
#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include <limits>
#include <type_traits>

#include <sys/stat.h>
#include "macro.h"
//

#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
#include <direct.h>	
#include <HttpExt.h>
#include <Nb30.h>
#else
#include <unistd.h>
#include <fcntl.h>
#endif

template <class T, size_t N>
inline size_t arraySize(const T (&)[N]) {
    return N;
}


#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
// UTF-8 to UTF-16 on Windows. Use just before API calls accepting strings
// nothrow = false: throws an exception on invalid input
// nothrow = true:  substitudes U+FFFD for invalid byte(s)
std::wstring Widen(const std::string s, bool nothrow = false);
inline std::wstring Widen(const char * s, bool nothrow = false) { return Widen(std::string(s), nothrow); }
// UTF-16 to UFT-8 on Windows. Use just after API calls returning strings
std::string Narrow(const std::wstring s, bool nothrow = false);
inline std::string Narrow(const wchar_t * s, bool nothrow = false) { return Narrow(std::wstring(s), nothrow); }
#else
// Does nothing on other platforms
inline std::string Widen(const std::string s, bool nothrow = false) { return s; }
inline std::string Widen(const char * s, bool nothrow = false) { return Widen(std::string(s)); }
inline std::string Narrow(const std::string s, bool nothrow = false) { return s; }
inline std::string Narrow(const char * s, bool nothrow = false) { return Narrow(std::string(s)); }
#endif


#if defined(_MSC_VER) && _MSC_VER < 1900
#define snprintf(buffer, buf_size, format, ...) _snprintf_s(buffer, buf_size, _TRUNCATE, format, ##__VA_ARGS__)
#endif


int strcmpDiscardCaps(const char *str1, const char *str2);


#endif
