var event = require('./event');

/**
 * 通常, 模块会在初始化时注册自己感兴趣的事件
 * 但这要求模块在启动时就被加载
 * 我们不希望某些模块占用启动时的资源消耗, 又希望在有对应事件产生时, 对应模块能收到消息
 *  所以由这个模块来负责lazy - require
 */

exports.check = function(e, owner, param){
	var mod;

	if(e == event.events.dialog.meta || e == event.events.dialog.newmeta){
		mod = require('../module/meta/manager');
	}else if(e == event.events.dialog.about){
		mod = require('../module/config/about');
	}else if(e == event.events.dialog.config){
		mod = require('../module/config/config');
	}else if(e == event.events.dialog.guide) {
		mod = require('../module/guide/ui');
	}else if(e == event.events.dialog.create) {
		mod = require('../module/guide/create');
	}else if(e == event.events.dialog.user) {
		mod = require('../module/user/dialog');
	}else if(e == event.events.file.upload || e == event.events.dialog.upload2){
		mod = require('../module/upload/manager');
	}else if(e == event.events.dialog.editmusic){
        mod = require('../module/sample/edit_music');
    }else if(e == event.events.file.exportMcz){
        mod = require('../module/note/export/export_mcz');
    }else if(e == event.events.note.simplify){
        mod = require('../module/note/simplify');
    }else if(e == event.events.grid.interval) {
        mod = require('../module/grid/dialog/min_interval');
    }else if(e == event.events.dialog.readme){
        mod = require('../module/tutorial/readme');
    }else if(owner == 'test'){
		mod = require('../module/test/manager');
	}else if(owner == 'tutorial'){
		mod = require('../module/tutorial/ui');
	}

	if(mod){
		mod.init();
		setTimeout(function(){
			event.trigger(e, param);
		}, 0);
	}
};