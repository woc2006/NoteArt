var ret = {
	KEY:{
		NUM_1: 49,
		NUM_2: 50,
		NUM_3: 51,
		NUM_4: 52,
		NUM_5: 53,
		NUM_6: 54,
		NUM_7: 55,
		NUM_8: 56,
		NUM_9: 57, 
		SPACE: 32,
		A: 65,
		C: 67,
		E: 69,
		F: 70,
		G: 71,
        H: 72,
		O: 79,
		S: 83,
		T: 84,
		U: 85,
		V: 86,
		Z: 90,
		F1: 112,
		F2: 113,
		F4: 115,
		F5: 116
	}
};

module.exports = ret;