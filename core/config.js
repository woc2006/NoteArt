var util = require('../util/index');
var Log = require('../util/logger');
var langDef = require('../data/i18n/en.json');
var fs = require('fs');

var _lang = null;

var ret = {
	ITEM:{
		LANG: "lang",
		VOL: "vol",
		EDIT_PLAY: 'edit_play',
		GUIDE_KEY: 'guide_key',
        GUIDE_CATCH: 'guide_catch',
        GUIDE_PAD:'guide_pad',
		MA:'ma',
		USER:'user',
		PSW:'psw',
		SID:'sid',
		UID:'uid',
		PROXY:'proxy',
		ADVANCE:'adv',
		PLAYSOUND:'plsd',
		GRID_ZOOM:'gr_zoom',
		GRID_DIVI:'gr_divi',
		MARKER_PATH:'marker_path'
	},

	init: function(){
		//加载用户配置
		//必须保证config是第一个执行的, 所以这里都是同步方法
		var path = util.getWorkingPath('./cache/config.json');
		this.config = util.loadJsonSync(path) || {};
		if(this.config.lang && this.config.lang != 'en'){
			_lang = require('../data/i18n/'+this.config.lang+'.json');  //如果不存在, 会报错
		}else{
			_lang = langDef;
		}
	},

	release: function(){
		if(!this.changed){
			return;
		}
		var path = util.getWorkingPath('./cache/config.json');
		util.saveJsonSync(path, this.config);
	},

	isDefaultLang: function () {
		return !this.config.lang || this.config.lang == 'en';
	},

	isLogin: function() {
		return this.config.user && this.config.sid && this.config.uid;
	},

	clearLogin: function(){
		this.config["user"] = "";
		this.config["psw"] = "";
		this.config["sid"] = "";
		this.config["uid"] = "";
	},

	update: function(key, val){
		this.config[key] = val;
		this.changed = true;
	},

	getVal: function(key){
		return this.config[key];
	},

	i18n: function(key){
		return _lang[key] || langDef[key] || '';
	},

	/**
	 * 获取主程序的路径, 默认跟editor同目录
	 * @returns {*}
	 */
	getMaPath: function(){
		var _path = this.config['ma'];
		if(_path){
			return _path;
		}
		_path = util.getWorkingPath('Malody.exe');
		if(fs.existsSync(_path)){
			return _path;
		}
		return null;
	},

	getServer: function(){
		return 'http://106.186.121.55';
		//return 'http://127.0.0.1:8000';
	},

	getHttpSession: function(){
		return ['uid=', this.config.uid, '&key=', this.config.sid, '&from=1'].join('');
	}
};

module.exports = ret;