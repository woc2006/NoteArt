var event = require('./core/event');
var menu = require('./module/menu/ui');
var welcome = require('./module/welcome/manager');
var gui = require("gui");
var io = require('./module/io/ui');
var size = require('./module/size/ui');
var util = require('./util/index');
var config = require('./core/config');
var path = require('path');
var Log = require('./util/logger');

var sound, input, note = null;

//这里只放首轮模块，即可能成为首轮用户操作触发点的模块
//非首轮的模块，由引用者负责初始化，每个模块都有防止反复init的保护
var start = Date.now();
size.resize();

//创建工作目录
util.mkdir(util.getWorkingPath("./cache"));
util.mkdir(util.getWorkingPath("./cache/log"));
config.init();

//audio.init();
sound = require('./module/audio/sound');
sound.init();

menu.init();
io.init();
//最后启动这个
welcome.init();

//不需要视觉的模块, 但需要用到, 延迟加载
setTimeout(function(){
	input = require('./core/input');
	input.init();
	var timeline = require('./module/timeline/ui');
	timeline.init();
	var grid = require('./module/grid/manager');
	grid.init();
	note = require('./module/note/manager');
	note.init();
	var layer = require('./module/layer/manager');
	layer.init();
	var status = require('./module/toggle/ui');
	status.init();
	var sample = require('./module/sample/manager');
	sample.init();
	var review = require('./module/review/manager');
	review.init();

	var test = require('./module/test/init');
	test.init();
}, 500);

//监听窗口级的drop事件
window.ondragover = function(e) { e.preventDefault(); return false };
window.ondrop = function(e) {
	e.preventDefault();
	event.trigger(event.events.global.drop, e);

	return false;
};

window.addEventListener('blur', function(){
	if(input){
		input.releaseKey();
	}
});

console.log("init time:"+(Date.now() - start));

var win = gui.Window.get();

win.on('close', function(){
	this.hide();
	welcome.release();
	config.release();
	if(sound){
		sound.release();
	}
	Log.i("Normal exit", "Exit");
	Log.release();
	this.close(true);
});

win.on('resize', function(){

});

win.on('enter-fullscreen', function(){

});

win.on('leave-fullscreen', function(){

});

process.on('uncaughtException', function (err) {
	Log.e(err.stack, "Exception");
});

